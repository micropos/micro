import 'package:flutter/material.dart';
import 'package:micro_pos/Api/splashApi.dart';
import 'package:micro_pos/pages/microLogin.dart';

class MicroSplash extends StatefulWidget {
  const MicroSplash({super.key});

  @override
  State<MicroSplash> createState() => _MicroSplashState();
}

class _MicroSplashState extends State<MicroSplash> {
  void getSplash() async {
    var splash = await getMySecond();
    if (splash) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => MicroLogin()));
    }
  }

  @override
  void initState() {
    super.initState();
    getSplash();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "MICRO POS",
              style: TextStyle(
                  fontSize: 40,
                  color: Color(0xff002D71),
                  fontWeight: FontWeight.w600),
            ),
    
          ],
        ),
      ),
    );
  }
}
