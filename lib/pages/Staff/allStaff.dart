import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Staff/addStaff.dart';
import 'package:micro_pos/pages/Customer/addCustomer.dart';

class AllStaff extends StatefulWidget {
  String? name;
  String? password;
  String? positon;

  AllStaff({this.name, this.password, this.positon});
  //const AllStaff({super.key});

  @override
  State<AllStaff> createState() => _AllStaffState();
}

class _AllStaffState extends State<AllStaff> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _customerTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: Text("All Staff"),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: staffList.length,
                itemBuilder: (context, index) => _staffDetail(
                    staffList[index].name.toString(),
                    staffList[index].position.toString(),
                    staffList[index].password.toString(),
                    index,
                    File(staffList[index].image!.path))),
            // File(customerList[index].image!.path)
          )
          // : Center(
          //     child: CircularProgressIndicator(),
          //   )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: mainColor,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddStaff(
                        title: "Add Staff",
                        buttonName: "Add",
                      )));
        },
      ),
    );
  }

  Widget _staffDetail(
      String sName, String sPassword, String sPositon, int index, File image) {
    // , File image
    return Container(
      padding: EdgeInsets.all(10),
      // height: 180,
      child: Card(
        color: Color(0xFFF3F1F1),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(90),
                child: Image.asset(
                  'assets/staff2.jpg',
                  height: 100.0,
                   width: 100.0,
                ),
              ),
              //  Container(
              //     width: 100.0,
              //     decoration: BoxDecoration(
              //       borderRadius: BorderRadius.all(Radius.circular(30)),
              //       // color: Colors.redAccent,
              //     ),
              //     child:
              // Image.asset(
              //       'assets/pf (1).jpg',
              //       height: 150.0,
              //       width: 100.0,
              //     )
              // ),
              // SizedBox(
              //     height: 100,
              //     width: 100,
              //     child: Image.asset(
              //       'assets/pf (1).jpg',
              //       width: 300,
              //     )
              //     //  CircleAvatar(
              //     //     child:
              //     //          Icon(
              //     //           Icons.man_3,
              //     //           size: 50,
              //     //         ),

              //     // )
              //     ),
            ),
            SizedBox(
              width: 5,
            ),
            Container(
              width: 180,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "${"Name = " + sName}",
                    style: TextStyle(color: mainColor, fontSize: 19),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "${"Positon = " + sPositon}",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
            // IconButton(
            //     onPressed: () {},
            //     icon: Icon(
            //       Icons.more_vert,
            //       size: 30,
            //     ))
            PopupMenuButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding: EdgeInsets.only(right: 1),
                child: Icon(
                  Icons.more_vert,
                  size: 30,
                ),
              ),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Edit"),
                    leading: Icon(Icons.edit),
                  ),
                  value: 'edit',
                ),
                // PopupMenuItem(
                //   child: ListTile(
                //     minLeadingWidth: 10,
                //     title: Text("Phone"),
                //     leading: Icon(Icons.phone),
                //   ),
                //   value: 'phone',
                // ),
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Delete"),
                    leading: Icon(Icons.delete),
                  ),
                  value: 'delete',
                ),
              ],
              onSelected: (value) {
                if (value == 'edit') {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddStaff(
                                title: "Edit",
                                buttonName: 'Edit',
                              )));
                } else if (value == 'delete') {
                  showCupertinoDialog(
                      context: context,
                      builder: (context) => _deleteDialog(index));
                }
              },
            )
          ],
        ),
      ),
    );
  }

  CupertinoAlertDialog _deleteDialog(int index) {
    return CupertinoAlertDialog(
      title: Text(
        "Delete!",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Are you sure you want to delete"),
      actions: [
        CupertinoDialogAction(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        CupertinoDialogAction(
          child: Text("OK"),
          onPressed: () {
            staffList.remove(index);
          },
        )
      ],
    );
  }
}
