import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/Model/fakeModel.dart';
import 'package:micro_pos/Model/staffMoel.dart';
import 'package:micro_pos/pages/Staff/allStaff.dart';
import 'package:micro_pos/pages/Customer/allCustomer.dart';

class AddStaff extends StatefulWidget {
  final String? title;
  final String? buttonName;
  AddStaff({this.title, this.buttonName});
  //const AddStaff({super.key});

  @override
  State<AddStaff> createState() => _AddStaffState();
}

class _AddStaffState extends State<AddStaff> {
  File? _imageFile;
  final _pickedImage = ImagePicker();

  Future getImage() async {
    final imagePick = await _pickedImage.pickImage(source: ImageSource.gallery);
    if (imagePick != null) {
      setState(() {
        _imageFile = File(imagePick.path);
      });
    }
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _staffNameController = TextEditingController();
  TextEditingController _staffPasswrdController = TextEditingController();
  TextEditingController _staffPositionController = TextEditingController();

  List<String> _cityList = ['Accountant', 'Office manager', 'Receptionist'];
  var _citySelected = "Office manager";

  @override
  Widget build(BuildContext context) {
    var _mSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.title}'),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Staff Name",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Staff Name can't be empty" : null,
                        controller: _staffNameController,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.person),
                            hintText: "Staff Name",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Staff Password",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Password can't be empty" : null,
                        controller: _staffPasswrdController,
                        obscureText: true,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.lock),
                            hintText: "Staff Password",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Position ",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        height: 60,
                        width: _mSize.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: textFieldColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            value: _citySelected,
                            items: _cityList.map((e) {
                              return DropdownMenuItem(
                                child: Text(e),
                                value: e,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _citySelected = value!;
                              });
                            },
                            icon: Icon(Icons.arrow_drop_down_sharp),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                "Customer Photo",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                    image: _imageFile == null
                        ? null
                        : DecorationImage(
                            image: FileImage(_imageFile ?? File('')),
                            fit: BoxFit.cover),
                    color: textFieldColor,
                    borderRadius: BorderRadius.circular(20)),
                //padding: EdgeInsets.all(10.0),
                width: double.infinity,
                height: 220,
                child: GestureDetector(
                    onTap: () {
                      getImage();
                    },
                    child: Image.asset('assets/addImage.png')),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                  width: double.infinity,
                  child: kAddButton('${widget.buttonName}', () {
                    if (_formKey.currentState!.validate()) {
                      staffList.add(StaffModel(
                          name: _staffNameController.text,
                          password: _staffPasswrdController.text,
                          position: _staffPositionController.text,
                          image: _imageFile));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllStaff(
                                    name: _staffNameController.text,
                                    password: _staffPasswrdController.text,
                                  )));
                    }
                  })),
            )
          ],
        ),
      ),
    );
  }
}
