import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/Model/fakeModel.dart';
import 'package:micro_pos/pages/Customer/allCustomer.dart';

class AddCustomer extends StatefulWidget {
  final String? title;
  final String? buttonName;
  AddCustomer({this.title, this.buttonName});
  //const AddCustomer({super.key});

  @override
  State<AddCustomer> createState() => _AddCustomerState();
}

class _AddCustomerState extends State<AddCustomer> {
  File? _imageFile;
  final _pickedImage = ImagePicker();

  Future getImage() async {
    final imagePick = await _pickedImage.pickImage(source: ImageSource.gallery);
    if (imagePick != null) {
      setState(() {
        _imageFile = File(imagePick.path);
      });
    }
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _customerNameController = TextEditingController();
  TextEditingController _customerPhoneController = TextEditingController();
  TextEditingController _customerEmailController = TextEditingController();
  TextEditingController _customerAddressController = TextEditingController();
  List<String> _townShipList = [
    'Thamine',
    'Bahan',
    'Sule',
    'Sanchaung',
    'HleDan'
  ];
  List<String> _cityList = ['Yangon', 'Mandalay', 'NyiPyiDaw'];
  var _citySelected = "Yangon";
  var _townShipSelected = "Bahan";

  @override
  Widget build(BuildContext context) {
    var _mSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.title}'),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Customer Name",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Customer Name can't be empty"
                            : null,
                        controller: _customerNameController,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.person),
                            hintText: "Customer Name",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Customer Phone",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) => value!.length < 9
                            ? "Phone have must least 9 characters"
                            : null,
                        controller: _customerPhoneController,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.phone),
                            hintText: "Customer Phone",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Customer Email",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Email can't be empty" : null,
                        controller: _customerEmailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.phone),
                            hintText: "Customer Email",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "City",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        height: 60,
                        width: _mSize.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: textFieldColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            value: _citySelected,
                            items: _cityList.map((e) {
                              return DropdownMenuItem(
                                child: Text(e),
                                value: e,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _citySelected = value!;
                              });
                            },
                            icon: Icon(Icons.arrow_drop_down_sharp),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "TownShip",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        height: 60,
                        width: _mSize.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: textFieldColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            hint: Text('Yangon'),
                            icon: Padding(
                              padding: const EdgeInsets.only(left: 40),
                              child: Icon(Icons.arrow_drop_down),
                            ),
                            value: _townShipSelected,
                            items: _townShipList.map((String e) {
                              return DropdownMenuItem<String>(
                                child: Text(e),
                                value: e,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _townShipSelected = value!;
                              });
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Customer Address",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Address can't be empty" : null,
                        controller: _customerAddressController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 6,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Padding(
                              padding: const EdgeInsets.only(bottom: 95),
                              child: Icon(Icons.location_history_outlined),
                            ),
                            hintText: "Customer Address",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      )
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                "Customer Photo",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                  decoration: BoxDecoration(
                      image: _imageFile == null
                          ? null
                          : DecorationImage(
                              image: FileImage(_imageFile ?? File('')),
                              fit: BoxFit.cover),
                      color: textFieldColor,
                      borderRadius: BorderRadius.circular(20)),
                  //padding: EdgeInsets.all(10.0),
                  width: double.infinity,
                  height: 220,
                  child: _imageFile == null
                      ? GestureDetector(
                          onTap: () {
                            getImage();
                          },
                          child: Image.asset('assets/addImage.png'))
                      : null),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                  width: double.infinity,
                  child: kAddButton('${widget.buttonName}', () {
                    if (_formKey.currentState!.validate()) {
                      // Navigator.of(context).pop();
                      customerList.add(Customer(
                          name: _customerNameController.text,
                          phone: _customerPhoneController.text,
                          email: _customerEmailController.text,
                          address: _customerAddressController.text,
                          image: _imageFile,
                          city: _citySelected,
                          townShip: _townShipSelected));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllCustomers()));
                      // Navigator.of(context).pop();
                    }
                  })),
            )
          ],
        ),
      ),
    );
  }
}
