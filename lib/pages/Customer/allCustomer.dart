import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Customer/addCustomer.dart';

class AllCustomers extends StatefulWidget {
  String? name;
  String? phone;
  String? email;
  String? address;

  AllCustomers({this.name, this.phone, this.email, this.address});
  //const AllCustomers({super.key});

  @override
  State<AllCustomers> createState() => _AllCustomersState();
}

class _AllCustomersState extends State<AllCustomers> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _customerTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: Text("All Customers"),
        centerTitle: false,
      ),
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                  controller: _customerTextController,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      hintText: "Search here...",
                      filled: true,
                      fillColor: textFieldColor,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none))),
            ),
          ),
          // customerList.length > 0
          Expanded(
            child: ListView.builder(
                itemCount: customerList.length,
                itemBuilder: (context, index) => _customerDetail(
                    customerList[index].name.toString(),
                    customerList[index].phone.toString(),
                    customerList[index].email.toString(),
                    customerList[index].address.toString(),
                    index,
                    File(customerList[index].image!.path),
                    // customerList[index].image.toString(),
                    customerList[index].townShip.toString(),
                    customerList[index].city.toString())),
            // File(customerList[index].image!.path)
          )
          // : Center(
          //     child: CircularProgressIndicator(),
          //   )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: mainColor,
        onPressed: () {
          // Navigator.of(context).pop();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddCustomer(
                        title: "Add Customer",
                        buttonName: "Add",
                      )));
        },
      ),
    );
  }

  Widget _customerDetail(String cName, String cPhone, String cEmail,
      String cAddress, int index, File? image, String townShip, String city) {
    // , File image
    return Container(
      padding: EdgeInsets.all(10),
      // height: 180,
      child: Card(
        color: Color(0xFFF3F1F1),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: SizedBox(
                  height: 100,
                  width: 100,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(90),
                      child: Image.file(
                        image!,
                        fit: BoxFit.cover,
                      ))),
            ),
            SizedBox(
              width: 5,
            ),
            Container(
              width: 180,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    cName,
                    style: TextStyle(color: mainColor, fontSize: 19),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    cPhone,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    cEmail,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text("${townShip},${city}"),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    cAddress,
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
            // IconButton(
            //     onPressed: () {},
            //     icon: Icon(
            //       Icons.more_vert,
            //       size: 30,
            //     ))
            PopupMenuButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding: EdgeInsets.only(right: 1),
                child: Icon(
                  Icons.more_vert,
                  size: 30,
                ),
              ),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Edit"),
                    leading: Icon(Icons.edit),
                  ),
                  value: 'edit',
                ),
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Phone"),
                    leading: Icon(Icons.phone),
                  ),
                  value: 'phone',
                ),
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Delete"),
                    leading: Icon(Icons.delete),
                  ),
                  value: 'delete',
                ),
              ],
              onSelected: (value) {
                if (value == 'edit') {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddCustomer(
                                title: "Edit",
                                buttonName: 'Edit',
                              )));
                } else if (value == 'delete') {
                  showCupertinoDialog(
                      context: context,
                      builder: (context) => _deleteDialog(index));
                }
              },
            )
          ],
        ),
      ),
    );
  }

  CupertinoAlertDialog _deleteDialog(int index) {
    return CupertinoAlertDialog(
      title: Text(
        "Delete!",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Are you sure you want to delete"),
      actions: [
        CupertinoDialogAction(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        CupertinoDialogAction(
          child: Text("OK"),
          onPressed: () {
            customerList.remove(index);
          },
        )
      ],
    );
  }
}
