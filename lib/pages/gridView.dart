// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:syncfusion_flutter_charts/charts.dart';

// class GridViews extends StatefulWidget {
//   const GridViews({super.key});

//   @override
//   State<GridViews> createState() => _GridViewsState();
// }

// class _GridViewsState extends State<GridViews> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Charts"),
//         centerTitle: false,
//       ),
//       body: Container(
//         height: 550,
//         child: SfCartesianChart(
//           title: ChartTitle(text: "Car Sales"),
//           primaryXAxis: CategoryAxis(title: AxisTitle(text: "Car Name")),
//           primaryYAxis: NumericAxis(title: AxisTitle(text: "Sales in Million")),
//           legend: Legend(isVisible: true),
//           series: <ChartSeries>[
//             ColumnSeries<SalesData, String>( //LineSeries//ColumnSeries
//                 name: "Cars",
//                 dataSource: getColumnData(),
//                 xValueMapper: (SalesData sales, _) => sales.x,
//                 yValueMapper: (SalesData sales, _) => sales.y,
//                 dataLabelSettings: DataLabelSettings(isVisible: true)),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class SalesData {
//   String x;
//   double y;

//   SalesData(this.x, this.y);
// }

// dynamic getColumnData() {
//   List<SalesData> columnData = [
//     SalesData("January", 8000000),
//     SalesData("February", 5000000),
//     SalesData("March", 1000000),
//     SalesData("April", 2000000),
//     SalesData("May", 2990000),
//     SalesData("June", 4000000),
//   ];

//   return columnData;
// }
// // import 'package:flutter/material.dart';
// // import 'package:flutter/src/widgets/framework.dart';
// // import 'package:flutter/src/widgets/placeholder.dart';
// // import 'package:syncfusion_flutter_charts/charts.dart';

// // class GridViews extends StatefulWidget {
// //   const GridViews({super.key});

// //   @override
// //   State<GridViews> createState() => _GridViewsState();
// // }

// // class _GridViewsState extends State<GridViews> {
// //   late List<Gdata> _chartData;
// //   late TooltipBehavior toolTipBehavior;

// //   @override
// //   void initState() {
// //     _chartData = _getCicularData();
// //     toolTipBehavior = TooltipBehavior(enable: true);
// //     super.initState();
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(
// //         title: Text("Carts"),
// //         centerTitle: true,
// //       ),
// //       body: Container(
// //         child: SfCircularChart(
// //           title: ChartTitle(text: "Car Sales"),
// //           legend: Legend(
// //               isVisible: true, overflowMode: LegendItemOverflowMode.wrap),
// //           tooltipBehavior: toolTipBehavior,
// //           series: <CircularSeries>[
// //             RadialBarSeries<Gdata, String>( //PieSeries //Doughnut
// //                 dataSource: _chartData,
// //                 xValueMapper: (Gdata x, _) => x.content,
// //                 yValueMapper: (Gdata y, _) => y.g,
// //                 dataLabelSettings: DataLabelSettings(isVisible: true))
// //           ],
// //         ),
// //       ),
// //     );
// //   }
// // }

// // class Gdata {
// //   final String content;
// //   final int g;

// //   Gdata(this.content, this.g);
// // }

// // List<Gdata> _getCicularData() {
// //   final List<Gdata> circularData = [
// //     Gdata("Lamborghini", 900000000),
// //     Gdata("Ferrari", 70000000),
// //     Gdata("Bugati", 9000000000),
// //     Gdata("Audi", 90000000000),
// //     Gdata("Mercedes", 900000000000),
// //   ];

// //   return circularData;
// // }

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class GridView extends StatefulWidget {
  const GridView({super.key});

  @override
  State<GridView> createState() => _GridViewState();
}

class _GridViewState extends State<GridView> {
  var preList = ["Myanmar",];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
