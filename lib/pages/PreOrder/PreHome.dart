import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/order_history.dart';
import 'package:micro_pos/pages/PreOrder/POS_PRE/preorder_pos.dart';
import 'package:micro_pos/pages/PreOrder/PreOrderProduct/FakePreOrder.dart';

class PreHome extends StatefulWidget {
  const PreHome({super.key});

  @override
  State<PreHome> createState() => _PreHomeState();
}

class _PreHomeState extends State<PreHome> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("PreOrder"),
        centerTitle: false,
        backgroundColor: preColor,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              highlightColor: Colors.white,
              splashColor: Colors.white,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FakePreOrder()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                height: size.height * 0.20,
                width: size.width * 0.2,
                child: Column(
                  children: [
                    Image.asset('assets/product.png'),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Product",
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              highlightColor: Colors.white,
              splashColor: Colors.white,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PreOrderPOSScreen()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                height: size.height * 0.20,
                width: size.width * 0.2,
                child: Column(
                  children: [
                    Image.asset('assets/pos.png'),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "POS",
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              highlightColor: Colors.white,
              splashColor: Colors.white,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OrderHistory()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                height: size.height * 0.20,
                width: size.width * 0.2,
                child: Column(
                  children: [
                    Image.asset('assets/preOrder.png'),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "OrderList",
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
