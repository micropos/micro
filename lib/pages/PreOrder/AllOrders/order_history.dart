import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/constant.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/order_detail.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/order_detail_edit.dart';

class OrderHistory extends StatefulWidget {
  const OrderHistory({super.key});

  @override
  State<OrderHistory> createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _productTextController = TextEditingController();

  List<Map> orderList = [
    {
      'name': 'iPhone 14 Pro Max',
      'cName': 'Mg Kyaw Kyaw',
      'cPhone': '09888777666',
      'cAddress': 'Kamayut, Yangon',
      'cDate': 'May 15,2023 2:10 PM',
      'image': 'assets/p1.jpg',
    },
    {
      'name': 'MacBook Air2',
      'cName': 'Mg Tun Tun',
      'cPhone': '09655777643',
      'cAddress': 'Hlaing, Yangon',
      'cDate': 'May 16,2023 3:08 PM',
      'image': 'assets/p2.jpg',
    },
    {
      'name': 'Canon M50',
      'cName': 'U Aung Gyi',
      'cPhone': '09979786866',
      'cAddress': 'Mandalay ',
      'cDate': 'May 14,2023 10:45 PM',
      'image': 'assets/p3.jpg',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order History'),
        backgroundColor: preColor,
      ),
      body: Column(
        children: [
          Column(
            children: [
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  height: 60,
                  child: TextFormField(
                    controller: _productTextController,
                    decoration: InputDecoration(
                        suffixIcon: Icon(Icons.search),
                        hintText: 'Search Here...',
                        hintStyle: TextStyle(height: 2.8),
                        filled: true,
                        fillColor: Color.fromARGB(255, 241, 246, 241),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none)),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: orderList
                  .map((e) => Order(
                      name: e['name'],
                      cName: e['cName'],
                      cPhone: e['cPhone'],
                      cAddress: e['cAddress'],
                      cDate: e['cDate'],
                      image: e['image']))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}

class Order extends StatefulWidget {
  String name, cName, cPhone, cAddress, cDate, image;

  Order(
      {required this.name,
      required this.cName,
      required this.cPhone,
      required this.cAddress,
      required this.cDate,
      required this.image});

  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => OrderDetail()));
        },
        child: Card(
          color: fillColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    widget.image,
                    width: 100,
                  ),
                ),
              ),
              const SizedBox(
                width: 1,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        widget.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: preColor,
                            fontSize: 18),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        widget.cName,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      Text(
                        widget.cPhone,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      Text(
                        widget.cAddress,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      Text(
                        widget.cDate,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      Container(
                        width: 200,
                        height: 33,
                        margin: EdgeInsets.symmetric(vertical: 3),
                        child: Row(
                          children: [
                            Container(
                                width: 70,
                                height: 25,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(border: Border.all()),
                                child: Text(
                                  'PICK UP',
                                  style: TextStyle(fontSize: 17),
                                )),
                            Container(
                                width: 50,
                                height: 25,
                                alignment: Alignment.center,
                                color: preColor,
                                child: Text(
                                  'CASH',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17),
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              PopupMenuButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Padding(
                  padding: EdgeInsets.only(right: 1),
                  child: Icon(Icons.more_vert),
                ),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: ListTile(
                      minLeadingWidth: 10,
                      title: Text("Edit"),
                      leading: Icon(Icons.edit),
                    ),
                    value: 'edit',
                  ),
                  PopupMenuItem(
                    child: ListTile(
                      minLeadingWidth: 10,
                      title: Text("Delete"),
                      leading: Icon(Icons.delete),
                    ),
                    value: 'delete',
                  ),
                ],
                onSelected: (value) {
                  if (value == 'edit') {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OrderDetailEdit()));
                  } else if (value == 'delete') {
                    if (Platform.isAndroid) {
                      showDialog(
                          context: context,
                          builder: (context) => simpleDialog(context));
                    } else {
                      showCupertinoDialog(
                          context: context,
                          builder: (context) => cupertinoDialog(context));
                    }
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
