import 'package:flutter/material.dart';
import 'package:micro_pos/pages/Main/HomePage.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/payment.dart';

import '../../../Components/constants.dart';

class OrderDetailEdit extends StatefulWidget {
  const OrderDetailEdit({super.key});

  @override
  State<OrderDetailEdit> createState() => _OrderDetailEditState();
}

class _OrderDetailEditState extends State<OrderDetailEdit> {
  int _counter = 1;
  void increase() {
    setState(() {
      _counter++;
    });
  }

  void decrease() {
    if (_counter > 0) {
      setState(() {
        _counter--;
      });
    }
  }

  List<String> customer = [
    'Mg Mg',
    'Kyaw Kyaw',
    'Su Su',
    'Aung Aung',
    'Phyo Phyo'
  ];
  List<String> pickUp = [
    'Pick Up',
    'Home Delivery',
    'Completed',
    'Cancelled',
    'Failed',
    'Expired',
    'Refuned'
  ];
  var macCounter = 1;
  var clothCounter = 1;
  final cash = ['KBZ PAY', 'WAVE PAY', 'CB PAY', 'AYA PAY'];
  String? valueCustomer;
  String? _checkOutSelected = "Pick Up";
  String? valueCash;

  final List<ChartData> chartData = [
    ChartData('+', 10),
  ];

  dynamic pay = 13.6 * 10;
  void _showDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text(
              'DELETE!',
              textAlign: TextAlign.center,
            ),
            content: const Text(
              'Are you sure you want to delete.',
              style: TextStyle(fontSize: 15),
              textAlign: TextAlign.center,
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Cancel',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Delete',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0XFF03600a),
        title: const Text('PreOrder'),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          Expanded(
            child: ListView(
              children: [_preEdit()],
            ),
          ),
          // _getDivider(),
          _getTotal(),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _preEdit() {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            textRow('PreOrder Id :', 'ADI589745189'),
            Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 3)),
                Text(
                  'Customer',
                  style: TextStyle(fontSize: 18),
                ),
                Padding(padding: EdgeInsets.only(left: 30)),
                _getCustomer(),
              ],
            ),
            // _getCustomer(),
            textRow('Product Info :', '1.test2'),
            Container(
              margin: EdgeInsets.only(left: 5),
              child: Row(
                children: [
                  Text(
                    'Qty',
                    style: TextStyle(fontSize: 18),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 290),
                    child: Row(
                      children: [
                        // Padding(padding: EdgeInsets.only(left: 20)),
                        GestureDetector(
                          onTap: () => decrease(),
                          child: Text(
                            '-',
                            style: TextStyle(fontSize: 20, color: Colors.red),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text(
                          '$_counter',
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        GestureDetector(
                          onTap: () => increase(),
                          child: Text(
                            '+',
                            style: TextStyle(fontSize: 20),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            textRow('Total Price :', '1000.0Ks'),
            Container(
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color.fromARGB(255, 237, 255, 225),
              ),
              child: Column(
                children: [
                  textRow('Date :', '19.1.2023'),
                  textRow('Payment Method :', 'CASH'),
                  textRow('Price :', '500.0Ks'),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget textRow(text1, text2, {color}) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: TextStyle(fontSize: 18, color: color),
          ),
          Text(
            text2,
            style: TextStyle(fontSize: 18, color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget _getTotal() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 30,
              width: 200,
              margin: const EdgeInsets.only(
                left: 10,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
                color: Colors.blueAccent,
              ),
              padding: EdgeInsets.only(left: 5),
              child: Row(
                children: [
                  Text(
                    'PICK UP',
                    style: TextStyle(fontSize: 14, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  Padding(padding: EdgeInsets.only(left: 5)),
                  Container(
                      height: 30,
                      width: 138,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        color: Colors.amber,
                      ),
                      // padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Container(
                            height: 30,
                            width: 13.8 * 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              color: preColor,
                            ),
                            padding: EdgeInsets.only(top: 5),
                            child: Text(
                              '${'\$ 90'}'.toString(),
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
            Container(),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Payment()));
                },
                child: Text('Add Pay +'))
          ],
        ),
        textRow('Sub Total :', '1000.0Ks', color: Colors.grey),
        textRow('Tax :', '0.00Ks', color: Colors.grey),
        textRow('Discount :', '0.00Ks', color: Colors.grey),
        textRow('Delivery Fee :', '0.00Ks', color: Colors.grey),
        Divider(
          color: Colors.black,
          thickness: 1,
        ),
        textRow('Total Price :', '1000.0Ks'),
        SizedBox(
          height: 60,
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
              width: double.infinity,
              child: PreAddButton("Confirm", () {
                Navigator.of(context).pop();
              })),
        )
      ],
    );
  }

  Widget _getCustomer() {
    return Container(
      width: 180,
      height: 50,
      margin: const EdgeInsets.only(
        left: 100,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200],
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          hint: const Text('Customer'),
          value: valueCustomer,
          icon: const Icon(Icons.arrow_drop_down),
          isExpanded: true,
          items: customer.map(customerMenu).toList(),
          onChanged: (value) => setState(
            () => this.valueCustomer = value,
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> customerMenu(String customer) => DropdownMenuItem(
        value: customer,
        child: Text(
          customer,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
