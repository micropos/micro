import 'package:flutter/material.dart';

const mainsColor = Color(0xFF002D71);

const Icon iosBackIcon = Icon(
  Icons.arrow_back_ios,
  color: Colors.white,
  size: 25,
);

const List<String> orderHistoryDropDownList = [
  'Paid',
  'Unpaid',
  'Gate',
  'Pending',
  'Complete',
  'Reject',
  'Pick up',
  'Home Delivery',
  'PreOrder',
  'PreOrder Home Delivery'
];
