import 'package:flutter/material.dart';
import 'package:micro_pos/pages/PreOrder/AllOrders/payment.dart';

import '../../../Components/constants.dart';

class PreOrderScreen extends StatefulWidget {
  const PreOrderScreen({super.key});

  @override
  State<PreOrderScreen> createState() => _PreOrderScreenState();
}

class _PreOrderScreenState extends State<PreOrderScreen> {
  final customer = ['Mg Mg', 'Kyaw Kyaw', 'Su Su', 'Aung Aung', 'Phyo Phyo'];
  final pickUp = [
    'Pick Up',
    'Home Delivery',
    'Completed',
    'Cancelled',
    'Failed',
    'Expired',
    'Refuned'
  ];
  final cash = ['KBZ PAY', 'WAVE PAY', 'CB PAY', 'AYA PAY'];
  String? valueCustomer;
  String? valuePickUp;
  String? valueCash;

  void _showDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text(
              'DELETE!',
              textAlign: TextAlign.center,
            ),
            content: const Text(
              'Are you sure you want to delete.',
              style: TextStyle(fontSize: 15),
              textAlign: TextAlign.center,
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Cancel',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Delete',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0XFF03600a),
        title: const Text('PreOrder'),
      ),
      body: Column(
        children: [
          _getCustomer(),
          const SizedBox(
            height: 5,
          ),
          Expanded(
            child: ListView(
              children: [
                _getProductCart(),
                _getProductCartOne(),
                _getProductCartTwo(),
                _getProductCartThree(),
              ],
            ),
          ),
          _getDivider(),
          _subTotal(),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _getProductCart() {
    return Card(
      child: Container(
        margin: const EdgeInsets.all(10),
        width: 407,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Stack(
          children: [
            Image.asset('assets/cart.png'),
            const Positioned(
              top: 10,
              left: 120,
              child: Text('Macbook Pro'),
            ),
            const Positioned(
              top: 40,
              left: 120,
              child: Text('Ks2000000'),
            ),
            Positioned(
              bottom: 60,
              right: 0,
              child: IconButton(
                onPressed: () {
                  _showDialog();
                },
                icon: const Icon(Icons.delete),
                color: Colors.red,
                splashColor: Colors.white,
                highlightColor: Colors.white,
              ),
            ),
            Positioned(
              left: 120,
              top: 70,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: const Color(0XFF03600a),
                ),
              ),
            ),
            Positioned(
              right: 140,
              top: 70,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: const Color(0XFF03600a),
                ),
              ),
            ),
            const Positioned(
              right: 143,
              top: 72,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            const Positioned(
              left: 123,
              top: 73,
              child: Icon(
                Icons.remove,
                color: Colors.white,
              ),
            ),
            const Positioned(
              top: 75,
              left: 175,
              child: Text(
                '1',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getCustomer() {
    return Container(
      width: 380,
      height: 50,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200],
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          hint: const Text('Customer'),
          value: valueCustomer,
          icon: const Icon(Icons.arrow_drop_down),
          isExpanded: true,
          items: customer.map(customerMenu).toList(),
          onChanged: (value) => setState(
            () => valueCustomer = value,
          ),
        ),
      ),
    );
  }

  Widget _getProductCartOne() {
    return SingleChildScrollView(
      child: Card(
        child: Container(
          margin: const EdgeInsets.all(10),
          width: 407,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Image.asset('assets/cloths.png'),
              const Positioned(
                top: 10,
                left: 120,
                child: Text('Cloth'),
              ),
              const Positioned(
                top: 40,
                left: 120,
                child: Text('Ks200000'),
              ),
              Positioned(
                bottom: 60,
                right: 0,
                child: IconButton(
                  onPressed: () {
                    _showDialog();
                  },
                  icon: const Icon(Icons.delete),
                  color: Colors.red,
                  splashColor: Colors.white,
                  highlightColor: Colors.white,
                ),
              ),
              Positioned(
                left: 120,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              Positioned(
                right: 140,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              const Positioned(
                right: 143,
                top: 73,
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                left: 123,
                top: 73,
                child: Icon(
                  Icons.remove,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                top: 75,
                left: 175,
                child: Text(
                  '1',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getProductCartTwo() {
    return SingleChildScrollView(
      child: Card(
        child: Container(
          margin: const EdgeInsets.all(10),
          width: 407,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Image.asset('assets/cloths.png'),
              const Positioned(
                top: 10,
                left: 120,
                child: Text('Cloth'),
              ),
              const Positioned(
                top: 40,
                left: 120,
                child: Text('Ks200000'),
              ),
              Positioned(
                bottom: 60,
                right: 0,
                child: IconButton(
                  onPressed: () {
                    _showDialog();
                  },
                  icon: const Icon(Icons.delete),
                  color: Colors.red,
                  splashColor: Colors.white,
                  highlightColor: Colors.white,
                ),
              ),
              Positioned(
                left: 120,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              Positioned(
                right: 140,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              const Positioned(
                right: 143,
                top: 73,
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                left: 123,
                top: 73,
                child: Icon(
                  Icons.remove,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                top: 75,
                left: 175,
                child: Text(
                  '1',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getProductCartThree() {
    return SingleChildScrollView(
      child: Card(
        child: Container(
          margin: const EdgeInsets.all(10),
          width: 407,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Image.asset('assets/cloths.png'),
              const Positioned(
                top: 10,
                left: 120,
                child: Text('Cloth'),
              ),
              const Positioned(
                top: 40,
                left: 120,
                child: Text('Ks200000'),
              ),
              Positioned(
                bottom: 60,
                right: 0,
                child: IconButton(
                  onPressed: () {
                    _showDialog();
                  },
                  icon: const Icon(Icons.delete),
                  color: Colors.red,
                  splashColor: Colors.white,
                  highlightColor: Colors.white,
                ),
              ),
              Positioned(
                left: 120,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              Positioned(
                right: 140,
                top: 70,
                child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color(0XFF03600a),
                  ),
                ),
              ),
              const Positioned(
                right: 143,
                top: 73,
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                left: 123,
                top: 73,
                child: Icon(
                  Icons.remove,
                  color: Colors.white,
                ),
              ),
              const Positioned(
                top: 75,
                left: 175,
                child: Text(
                  '1',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getDivider() {
    return Divider(
      height: 50,
      color: Colors.grey[300],
      thickness: 1,
      indent: 10,
      endIndent: 10,
    );
  }

  Widget _subTotal() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey[200],
      ),
      margin: const EdgeInsets.only(left: 10, right: 8),
      height: 300,
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0XFF03600a),
                      ),
                      onPressed: () {},
                      child: const Text('Pick Up'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0XFF03600a),
                      ),
                      onPressed: () {},
                      child: const Text('100%'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0XFF03600a),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Payment(),
                          ),
                        );
                      },
                      child: const Text('Add Payment'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Sub Total'),
              trailing: Text('Ks2252000'),
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Total Tax'),
              trailing: Text('Ks2000'),
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Total'),
              trailing: Text('Ks2254000'),
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Balance'),
              trailing: Text('Ks1000000'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: 200,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: const Color(0XFF03600a),
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('Close'),
                      ),
                    ],
                    title: const Text(
                      'PAYMENT SUCCESS!',
                      textAlign: TextAlign.center,
                    ),
                    content:
                        Image.asset('assets/true.png', width: 100, height: 100),
                  ),
                );
              },
              child: const Text('Submit'),
            ),
          )
        ],
      ),
    );
  }

  DropdownMenuItem<String> customerMenu(String customer) => DropdownMenuItem(
        value: customer,
        child: Text(
          customer,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> pickUpMenu(String pickUp) => DropdownMenuItem(
        value: pickUp,
        child: Text(
          pickUp,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> cashMenu(String cash) => DropdownMenuItem(
        value: cash,
        child: Text(
          cash,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
