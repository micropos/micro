import 'package:flutter/material.dart';
import 'package:micro_pos/pages/PreOrder/POS_PRE/preorder_screen.dart';

import '../../../Components/constants.dart';

class Payment extends StatefulWidget {
  const Payment({super.key});

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final cash = ['KBZ PAY', 'WAVE PAY', 'CB PAY', 'AYA PAY'];
  String? valueCash;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pamyment'),
        backgroundColor: const Color(0XFF03600a),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 20, top: 15),
            child: Text(
              'Amount',
              style: TextStyle(fontSize: 16),
            ),
          ),
          SizedBox(
            height: 60,
            child: Form(
              child: Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                child: TextFormField(
                    decoration: InputDecoration(
                        hintText: "Amount",
                        filled: true,
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide.none))),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 20, top: 15),
            child: Text(
              'Cash',
              style: TextStyle(fontSize: 16),
            ),
          ),
          _getCash(),
          const SizedBox(
            height: 20,
          ),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xFF03600a)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PreOrderScreen(),
                  ),
                );
              },
              child: const Text('Add'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getCash() {
    return Container(
      width: 400,
      height: 50,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200],
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          hint: const Text('Cash'),
          value: valueCash,
          icon: const Icon(Icons.arrow_drop_down),
          isExpanded: true,
          items: cash.map(cashMenu).toList(),
          onChanged: (value) => setState(
            () => valueCash = value,
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> cashMenu(String cash) => DropdownMenuItem(
        value: cash,
        child: Text(
          cash,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
