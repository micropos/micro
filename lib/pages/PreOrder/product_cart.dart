
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';


class ProductCart extends StatefulWidget {
  const ProductCart({super.key});

  @override
  State<ProductCart> createState() => _ProductCartState();
}

class _ProductCartState extends State<ProductCart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        centerTitle: false,

        title: Text(
          'Product Cart',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 80,
          ),
          Center(child: Image.asset('assets/manCart.png')),
          SizedBox(height: 60,),
          const Text(
            'No Product in Cart',
            style: TextStyle(fontSize: 25, color: Color(0xff002D71)),
          )
        ],
      ),
    );
  }
}
