import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';


class QROrBarcodeScanner extends StatefulWidget {
  const QROrBarcodeScanner({super.key});

  @override
  State<QROrBarcodeScanner> createState() => _QROrBarcodeScannerState();
}

class _QROrBarcodeScannerState extends State<QROrBarcodeScanner> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:mainColor,
        centerTitle: false,
        title: Text(
          'QR or Barcode Scanner',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
    );
  }
}
