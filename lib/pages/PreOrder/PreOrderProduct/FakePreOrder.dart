import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../../Components/constants.dart';
import 'AddPreOrder.dart';

class FakePreOrder extends StatefulWidget {
  String? name;
  String? sell;
  String? image;

  FakePreOrder({this.name, this.sell, this.image});

  @override
  State<FakePreOrder> createState() => _FakePreOrderState();
}

class _FakePreOrderState extends State<FakePreOrder> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _productTextController = TextEditingController();

  List<Map> productList = [
    {
      'name': 'Iphone 14 Pro Max',
      'sell': 'Ks 3650000',
      'image': 'assets/p1.jpg',
    },
    {
      'name': 'MacBook Air 2',
      'sell': 'Ks 4150000',
      'image': 'assets/p2.jpg',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PreOrder Product"),
        backgroundColor: preColor,
      ),
      body: Column(
        children: [
          Column(
            children: [
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  height: 60,
                  child: TextFormField(
                    controller: _productTextController,
                    decoration: InputDecoration(
                        suffixIcon: Icon(Icons.search),
                        hintText: 'Search Here...',
                        hintStyle: TextStyle(height: 2.8),
                        filled: true,
                        fillColor: preFill,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none)),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: productList
                  .map((e) => PreProduct(
                      name: e['name'], sell: e['sell'], image: e['image']))
                  .toList(),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: preColor,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => AddPreOrderProduct()));
        },
      ),
    );
  }
}

class PreProduct extends StatefulWidget {
  String name, sell, image;

  PreProduct(
      {super.key, required this.name, required this.sell, required this.image});

  @override
  State<PreProduct> createState() => _PreProductState();
}

class _PreProductState extends State<PreProduct> {
  int index = 0;
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7),
      child: Card(
        color: preFill,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                widget.image,
                width: 80,
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      widget.name,
                      style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          color: preColor,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Sell Price : ${widget.sell}',
                      style: const TextStyle(color: Colors.black, fontSize: 15),
                    ),
                  ],
                ),
              ),
            ),
            PopupMenuButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding: EdgeInsets.only(right: 1),
                child: Icon(Icons.more_vert),
              ),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Edit"),
                    leading: Icon(Icons.edit),
                  ),
                  value: 'edit',
                ),
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Detail"),
                    leading: Icon(Icons.details),
                  ),
                  value: 'detail',
                ),
                PopupMenuItem(
                  child: ListTile(
                    minLeadingWidth: 10,
                    title: Text("Delete"),
                    leading: Icon(Icons.delete),
                  ),
                  value: 'delete',
                ),
              ],
              onSelected: (value) {
                if (value == 'edit') {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddPreOrderProduct()));
                } else if (value == 'detail') {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => PreOrderProductDetail()));
                } else if (value == 'delete') {
                  // if (Platform.isAndroid) {
                  //   showDialog(
                  //       context: context,
                  //       builder: (context) => simpleDialog(context));
                  // } else {
                  //   showCupertinoDialog(
                  //       context: context,
                  //       builder: (context) => cupertinoDialog(context));
                  // }
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
