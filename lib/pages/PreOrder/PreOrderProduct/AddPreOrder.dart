import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_pos/pages/PreOrder/PreOrderProduct/FakePreOrder.dart';
import 'package:micro_pos/pages/Product/FkScreen.dart';

import '../../../Components/constants.dart';

class AddPreOrderProduct extends StatefulWidget {
  AddPreOrderProduct({super.key});

  @override
  State<AddPreOrderProduct> createState() => _AddPreOrderProductState();
}

class _AddPreOrderProductState extends State<AddPreOrderProduct> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _productNameController = TextEditingController();
  // TextEditingController _productCategoryController = TextEditingController();
  TextEditingController _productDescriptionController = TextEditingController();
  TextEditingController _productSellController = TextEditingController();
  TextEditingController _productWeightController = TextEditingController();
  // TextEditingController _productWeightUnitController = TextEditingController();

  File? _imageFile;
  final _picker = ImagePicker();
  final category = ['Phone', 'Laptop', 'Keyboard', 'Charger', 'Earphone'];
  final weight = ['ml', 'pcs', 'L', 'Kg', 'g'];
  String? valueCategories;
  String? valueWeight;

  Future getImage() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.gallery);
    if (PickedFile != null) {
      setState(() {
        _imageFile = File(PickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add PreOrder Product'),
        backgroundColor: preColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(5),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Name"
                            : null,
                        controller: _productNameController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: preFill,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          prefixIcon: Icon(Icons.person),
                          hintText: "Product Name",
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: preFill,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            hint: Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 7),
                                  child: Icon(
                                    Icons.category,
                                    color: Color.fromARGB(255, 136, 135, 135),
                                  ),
                                ),
                                Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 4),
                                    child: const Text('Product Categories')),
                              ],
                            ),
                            value: valueCategories,
                            icon: const Icon(Icons.arrow_drop_down),
                            isExpanded: true,
                            items: category.map(categoryMenu).toList(),
                            onChanged: (value) => setState(
                              () => this.valueCategories = value,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Description"
                            : null,
                        controller: _productDescriptionController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 6,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: preFill,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(10)),
                          prefixIcon: Padding(
                            padding: const EdgeInsets.only(bottom: 95),
                            child: Icon(Icons.description),
                          ),
                          hintText: "Product Description",
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Sell Price"
                            : null,
                        controller: _productSellController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: preFill,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          prefixIcon: Icon(Icons.price_change),
                          hintText: "Product Sell Price",
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Weight"
                            : null,
                        controller: _productWeightController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: preFill,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          prefixIcon: Icon(Icons.monitor_weight),
                          hintText: "Product Weight",
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: preFill,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            hint: Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 7),
                                  child: Icon(
                                    Icons.monitor_weight,
                                    color: Color.fromARGB(255, 136, 135, 135),
                                  ),
                                ),
                                Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 4),
                                    child: const Text(
                                        'Select Product Weight Unit')),
                              ],
                            ),
                            value: valueWeight,
                            icon: const Icon(Icons.arrow_drop_down),
                            isExpanded: true,
                            items: weight.map(weightMenu).toList(),
                            onChanged: (value) => setState(
                              () => this.valueWeight = value,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                          width: double.infinity,
                          child: TextButton(
                              child: Text(
                                'Choose PreOrder Product Image',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                // if (_formKey.currentState!.validate()) {
                                //   // getImage();
                                // }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) => preColor),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                            decoration: BoxDecoration(
                                image: _imageFile == null
                                    ? null
                                    : DecorationImage(
                                        image:
                                            FileImage(_imageFile ?? File('')),
                                        fit: BoxFit.cover),
                                color: preFill,
                                borderRadius: BorderRadius.circular(20)),
                            //padding: EdgeInsets.all(10.0),
                            width: double.infinity,
                            height: 220,
                            child: _imageFile == null
                                ? GestureDetector(
                                    onTap: () {
                                      getImage();
                                    },
                                    child: Icon(Icons.add_a_photo))
                                // child: Image.asset('assets/addImage.png'))
                                : null),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: SizedBox(
                            width: double.infinity,
                            child: TextButton(
                              child: Text(
                                "Add",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  productList.add(
                                    FakePreOrder(
                                      name: _productNameController.text,
                                      sell: _productSellController.text,
                                    ),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FakePreOrder(
                                          // name: _productNameController.text,
                                          // sell: _productSellController.text,
                                          ),
                                    ),
                                  );
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) => preColor),
                              ),
                            ),
                          )),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }

  DropdownMenuItem<String> categoryMenu(String category) => DropdownMenuItem(
        value: category,
        child: Text(
          category,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> weightMenu(String weight) => DropdownMenuItem(
        value: weight,
        child: Text(
          weight,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
