import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/DeliveryFee/deliveryfee.dart';

class DeliveryAdd extends StatefulWidget {
  const DeliveryAdd({super.key});

  @override
  State<DeliveryAdd> createState() => _DeliveryAddState();
}

class _DeliveryAddState extends State<DeliveryAdd> {
  List<String> _townShipList = [
    'Thamine',
    'Bahan',
    'Sule',
    'Sanchaung',
    'HleDan'
  ];
  List<String> _cityList = ['Yangon', 'Mandalay', 'NyiPyiDaw'];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _priceController = TextEditingController();

  var _citySelected = "Yangon";
  var _townShipSelected = "Bahan";

  @override
  Widget build(BuildContext context) {
    Size msize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Delivery"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Text(
              "City",
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: msize.width,
            padding: EdgeInsets.symmetric(horizontal: 20),
            margin: EdgeInsets.symmetric(horizontal: 15),
            //padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
                color: textFieldColor, borderRadius: BorderRadius.circular(20)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                value: _citySelected,
                items: _cityList.map((e) {
                  return DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    _citySelected = value!;
                  });
                },
                icon: Icon(Icons.arrow_drop_down_sharp),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Text(
              "TownShip",
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              width: msize.width,
              padding: EdgeInsets.symmetric(horizontal: 20),
              margin: EdgeInsets.symmetric(horizontal: 15),
              //padding: EdgeInsets.symmetric(horizontal: 5),
              decoration: BoxDecoration(
                  color: textFieldColor,
                  borderRadius: BorderRadius.circular(20)),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  hint: Text('Yangon'),
                  icon: Padding(
                    padding: const EdgeInsets.only(left: 40),
                    child: Icon(Icons.arrow_drop_down),
                  ),
                  value: _townShipSelected,
                  items: _townShipList.map((String e) {
                    return DropdownMenuItem<String>(
                      child: Text(e),
                      value: e,
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      _townShipSelected = value!;
                    });
                  },
                ),
              )),

          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28),
            child: Text(
              "Price",
              style: TextStyle(fontSize: 20),
            ),
          ),
          // SizedBox(
          //   height: 4,
          // ),
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 9.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                    controller: _priceController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 15, top: 29),
                        // EdgeInsets.symmetric(hori, vertical: 18),

                        suffixIcon: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Icon(Icons.monetization_on),
                        ),
                        prefixStyle: TextStyle(color: Colors.green),
                        hintText: "Price",
                        filled: true,
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide.none))),
              ),
            ),
          ),
          SizedBox(
            height: 35,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              width: msize.width,
              child: kAddButton("Add", () {
                Navigator.of(context).pop();
              }),
            ),
          )
        ],
      ),
    );
  }
}
