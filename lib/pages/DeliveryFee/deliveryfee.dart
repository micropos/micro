import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/Model/deliveryModel.dart';
import 'package:micro_pos/pages/DeliveryFee/deliveryAdd.dart';

class DeliveryFee extends StatefulWidget {
  const DeliveryFee({super.key});

  @override
  State<DeliveryFee> createState() => _DeliveryFeeState();
}

class _DeliveryFeeState extends State<DeliveryFee> {
  @override
  Widget build(BuildContext context) {
    var msize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Delivery"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              "Delivery Lists",
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: deliveyList.length,
              itemBuilder: (context, index) {
                final DeliveryModel items = deliveyList[index];
                return Dismissible(
                  key: UniqueKey(),
                  onDismissed: (DismissDirection dir) {
                    setState(() {
                      deliveyList.removeAt(index);
                    });
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                          dir == DismissDirection.endToStart ? "Dlete" : ""),
                      action: SnackBarAction(
                        label: "Undo",
                        onPressed: () {
                          setState(() {
                            deliveyList.insert(index, items);
                          });
                        },
                      ),
                    ));
                  },
                  background: Container(
                    color: Colors.red,
                    alignment: Alignment.centerRight,
                    child: Icon(Icons.delete),
                  ),
                  child: _getListTile(deliveyList[index].townShip,
                      deliveyList[index].city, deliveyList[index].fees),
                );
              },
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => DeliveryAdd()));
        },
        child: Icon(Icons.add),
        backgroundColor: mainColor,
      ),
    );
  }

  Widget _getListTile(n1, n2, n3) {
    return Card(
      child: InkWell(
        onTap: () {},
        splashColor: Color.fromARGB(255, 92, 92, 92),
        child: ListTile(
          title: Text("$n1,$n2"),
          subtitle: Text("$n3"),
        ),
      ),
    );
  }
}
