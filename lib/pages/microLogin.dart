import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Main/HomePage.dart';
import 'package:micro_pos/pages/response.dart';

class MicroLogin extends StatefulWidget {
  const MicroLogin({super.key});

  @override
  State<MicroLogin> createState() => _MicroLoginState();
}

class _MicroLoginState extends State<MicroLogin> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size lsize = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      "Login",
                      style: TextStyle(
                          fontSize: 40,
                          color: Color(0xff002D71),
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(
                    width: lsize.width - 233,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 1),
                    child: Image.asset('assets/circle2.png'),
                  ),
                  SizedBox(
                    width: 26,
                  ),
                  Image.asset('assets/circle3.png')
                ],
              ),
              SizedBox(
                height: 170,
              ),
              Image.asset(
                'assets/MicroLogo.png',
                scale: 7,
              ),
              SizedBox(
                height: 60,
              ),
              Center(
                child: Expanded(
                  child: Form(
                    key: _key,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          child: TextFormField(
                            controller: _emailController,
                            decoration: inputDecorations(
                                "Email",
                                Icon(
                                  Icons.email,
                                  color: mainColor,
                                )),
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                          child: TextFormField(
                            obscureText: true,
                            controller: _passwordController,
                            decoration: inputDecorations(
                                "Password",
                                Icon(
                                  Icons.lock,
                                  color: mainColor,
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 100,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: buttonColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onPressed: () {
                          print("Size is ${lsize.width}");
                          print("Size is ${lsize.height}");
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomePage()));
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(color: buttonTextColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox( //0.13
                // height: lsize.height * 0.13
                height: lsize.height * 0.13
                  // height: 50,
                  ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/circle.png'),
                  SizedBox(
                    width: 26,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 9),
                    child: Image.asset(
                      'assets/circle2.png',
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    ));
  }
}
