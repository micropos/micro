import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Settings/Payment/addPayment.dart';
import 'package:micro_pos/pages/Settings/Payment/payment.dart';
import 'package:micro_pos/pages/Settings/categories.dart';
import 'package:micro_pos/pages/response.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              highlightColor: Colors.white,
              splashColor: Colors.white,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Categories()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                height: size.height * 0.30,
                width: size.width * 0.3,
                child: Column(
                  children: [
                    Image.asset('assets/categories.png'),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Categories",
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              highlightColor: Colors.white,
              splashColor: Colors.white,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PayMent()));
              },
              child: Container(
                height: size.height * 0.30,
                width: size.width * 0.3,
                child: Column(
                  children: [
                    Image.asset('assets/payment.png'),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Payment\nmethod:",
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
