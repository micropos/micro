import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Settings/Payment/addPayment.dart';

class PayMent extends StatefulWidget {
  const PayMent({super.key});

  @override
  State<PayMent> createState() => _PayMentState();
}

class _PayMentState extends State<PayMent> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _paymentController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                  controller: _paymentController,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      hintText: "Search here...",
                      filled: true,
                      fillColor: textFieldColor,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none))),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                //padding: EdgeInsets.all(5.0),
                width: 180,
                child: Card(
                  color: buttonColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      Image.asset(
                        'assets/kpay.png',
                        width: 100,
                        height: 100,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Kpay",
                              style: TextStyle(fontSize: 20),
                            ),
                            InkWell(
                              onTap: () {
                                showCupertinoDialog(
                                    context: context,
                                    builder: (context) => _deleteDialog());
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                //padding: EdgeInsets.all(5.0),
                width: 180,
                child: Card(
                  color: buttonColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      Image.asset(
                        'assets/wavePay.png',
                        width: 100,
                        height: 100,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Wavepay",
                              style: TextStyle(fontSize: 20),
                            ),
                            InkWell(
                              onTap: () {
                                showCupertinoDialog(
                                    context: context,
                                    builder: (context) => _deleteDialog());
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddPayment()));
        },
        child: Icon(Icons.add),
      ),
    );
  }

  CupertinoAlertDialog _deleteDialog() {
    return CupertinoAlertDialog(
      title: Text(
        "Delete!",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Are you sure you want to delete"),
      actions: [
        CupertinoDialogAction(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        CupertinoDialogAction(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
