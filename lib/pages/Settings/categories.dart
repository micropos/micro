import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Settings/addCategories.dart';
import 'package:micro_pos/pages/Settings/editCategories.dart';

class Categories extends StatefulWidget {
  const Categories({super.key});

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Categories"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: ListView(
        children: [
          InkWell(
            highlightColor: Colors.white,
            splashColor: Colors.white,
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => EditCategories()));
            },
            child: Container(
              padding: EdgeInsets.all(10),
              height: size.height * 0.15,
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.blueGrey[100],
                child: Row(
                  children: [
                    Image.asset(
                      'assets/shirt.png',
                      width: 150,
                      height: size.height * 0.15,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "T-Shirt",
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                ),
              ),
            ),
          ),
          InkWell(
            highlightColor: Colors.white,
            splashColor: Colors.white,
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(10),
              height: size.height * 0.15,
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.blueGrey[100],
                child: Row(
                  children: [
                    Image.asset(
                      'assets/sneakers.png',
                      width: 150,
                      height: size.height * 0.15,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "Shoes",
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AddCategories()));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
