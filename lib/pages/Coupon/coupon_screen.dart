import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Coupon/couponList.dart';

class CouponScreen extends StatefulWidget {
  final String? title;
  CouponScreen({this.title});
  // const CouponScreen({super.key});

  @override
  State<CouponScreen> createState() => _CouponScreenState();
}

class _CouponScreenState extends State<CouponScreen> {
  final couponType = ['Default', 'Coupon 1', 'Coupon 2', 'Coupon 3'];
  final discountType = ['Percent', 'Discount 1', 'Discount 2', 'Discount 3'];

  String? valueCoupon;
  String? valueDiscount;

  DateTime _dateTime1 = DateTime.now();
  DateTime _dateTime2 = DateTime.now();

  void _startDate() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2099),
    ).then((value) {
      setState(() {
        _dateTime1 = value!;
      });
    });
  }

  void _expiredDate() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2099),
    ).then((value) {
      setState(() {
        _dateTime2 = value!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size mSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: Text('${widget.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 20, top: 15),
              child: Text(
                'Name',
                style: TextStyle(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 60,
              child: Form(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                  child: TextFormField(
                      decoration: InputDecoration(
                          hintText: "Name",
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide.none))),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Member Code',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 60,
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                      child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "Member Code",
                              hintStyle: const TextStyle(height: 1),
                              filled: true,
                              fillColor: textFieldColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Coupon Type',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                  width: mSize.width,
                  height: 50,
                  margin: const EdgeInsets.only(left: 8, right: 8, top: 10),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 12,
                    vertical: 4,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey[200],
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: const Text('Coupon Type'),
                      value: valueCoupon,
                      icon: const Icon(Icons.arrow_drop_down),
                      isExpanded: true,
                      items: couponType.map(couponMenu).toList(),
                      onChanged: (value) => setState(
                        () => valueCoupon = value,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Limit for same user',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 60,
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                      child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "EX:10",
                              filled: true,
                              fillColor: textFieldColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Start Date',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                ListTile(
                  title: Container(
                    width: 380,
                    height: 51,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.grey[200],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 17, left: 10),
                      child: Text(
                        '${_dateTime1.day}-${_dateTime1.month}-${_dateTime1.year}',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  trailing: MaterialButton(
                    onPressed: _startDate,
                    child: const Icon(Icons.calendar_month),
                  ),
                )
                // Container(
                //   width: 350,
                //   height: 51,
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(20),
                //     color: Colors.grey[200],
                //   ),
                //   child: Padding(
                //     padding: const EdgeInsets.only(top: 17, left: 10),
                //     child: Text(
                //       '${_dateTime1.day}-${_dateTime1.month}-${_dateTime1.year}',
                //       style: const TextStyle(fontSize: 16),
                //     ),
                //   ),
                // ),
                // MaterialButton(
                //   onPressed: _startDate,
                //   child: const Icon(Icons.calendar_month),
                // ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Expired Date',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                ListTile(
                  title: Container(
                    width: 250,
                    height: 51,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.grey[200],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 17, left: 10),
                      child: Text(
                        '${_dateTime2.day}-${_dateTime2.month}-${_dateTime2.year}',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  trailing: MaterialButton(
                    onPressed: _expiredDate,
                    child: const Icon(Icons.calendar_month),
                  ),
                )
                // Container(
                //   width: 250,
                //   height: 51,
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(20),
                //     color: Colors.grey[200],
                //   ),
                //   child: Padding(
                //     padding: const EdgeInsets.only(top: 17, left: 10),
                //     child: Text(
                //       '${_dateTime2.day}-${_dateTime2.month}-${_dateTime2.year}',
                //       style: const TextStyle(fontSize: 16),
                //     ),
                //   ),
                // ),
                // MaterialButton(
                //   onPressed: _expiredDate,
                //   child: const Icon(Icons.calendar_month),
                // ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Min Purchase',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 60,
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                      child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "0",
                              filled: true,
                              fillColor: textFieldColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Max Discount',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 60,
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                      child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "0",
                              filled: true,
                              fillColor: textFieldColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Discount',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 60,
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                      child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "Discount",
                              filled: true,
                              fillColor: textFieldColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 15),
                  child: Text(
                    'Discount Type',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                  width: mSize.width,
                  height: 50,
                  margin: const EdgeInsets.only(left: 8, right: 8, top: 10),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 12,
                    vertical: 4,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey[200],
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: const Text('Discount Type'),
                      value: valueDiscount,
                      icon: const Icon(Icons.arrow_drop_down),
                      isExpanded: true,
                      items: discountType.map(discountMenu).toList(),
                      onChanged: (value) => setState(
                        () => valueDiscount = value,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Container(
                  width: mSize.width,
                  child: kAddButton("Submit", () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CouponListScreen()));
                  })),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  DropdownMenuItem<String> couponMenu(String coupon) => DropdownMenuItem(
        value: coupon,
        child: Text(
          coupon,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> discountMenu(String discount) => DropdownMenuItem(
        value: discount,
        child: Text(
          discount,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
