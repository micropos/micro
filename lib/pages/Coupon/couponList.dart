import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/cupertino/dialog.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Coupon/coupon_screen.dart';

class CouponListScreen extends StatefulWidget {
  const CouponListScreen({super.key});

  @override
  State<CouponListScreen> createState() => _CouponListScreenState();
}

class _CouponListScreenState extends State<CouponListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: const Text('Coupon List'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.all(10.0),
            ),
            _getListTile("ABC", '98765465432', 'Default'),
            _getListTile("BGH", '4665643432', 'Default'),
            _getListTile("HGF", '44345445', 'Default'),
            _getListTile("TRE", '98775632', 'Default'),
            _getListTile("KJH", '34534545', 'Default'),
            _getListTile("ETR", '34534523', 'Default'),
            // _getListTileOne(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) =>  CouponScreen(title: "Add Coupon",)));
        },
        backgroundColor: mainColor,
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _getListTile(String n1, n2, n3) {
    return Card(
      child: InkWell(
        onTap: () {},
        splashColor: Color.fromARGB(255, 92, 92, 92),
        child: ListTile(
            title: Text("$n1, $n2"),
            subtitle: Text("$n3"),
            trailing: Container(
              child: PopupMenuButton(
                child: Icon(Icons.more_vert),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: Text("Edit"),
                    value: 'edit',
                  ),
                  PopupMenuItem(
                    child: Text("Delete"),
                    value: 'delete',
                  ),
                ],
                onSelected: (value) {
                  if (value == 'edit') {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => CouponScreen(
                              title: "Edit Coupon",
                            )));
                  } else {
                    if (Platform.isAndroid) {
                      showDialog(
                          context: context,
                          builder: (context) => simpleDialog(context));
                    } else {
                      showCupertinoDialog(
                          context: context,
                          builder: (context) => cupertinoDialog(context));
                    }
                  }
                },
              ),
            )),
      ),
    );
  }

  void showCupertinoDialog({required BuildContext context, required CupertinoAlertDialog Function(dynamic context) builder}) {}

}
