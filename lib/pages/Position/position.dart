import 'package:flutter/material.dart';
import 'package:micro_pos/pages/Position/position_list_screen.dart';
import 'package:micro_pos/pages/Report/report.dart';

class PositionScreen extends StatefulWidget {
  final String? title;
  PositionScreen({this.title});
  // const PositionScreen({super.key});

  @override
  State<PositionScreen> createState() => _PositionScreenState();
}

class _PositionScreenState extends State<PositionScreen> {
  List<String> _seleteRole = [];

  void _showPositionRole() async {
    final List<String> roles = [
      'Role 1',
      'Role 2',
      'Role 3',
      'Role 4',
      'Role 5',
    ];

    final List<String>? results = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return MultiSelect(
          items: roles,
        );
      },
    );

    if (results != null) {
      setState(() {
        _seleteRole = results;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('${widget.title}'),
        backgroundColor: mainColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 20, top: 15),
            child: Text(
              'Name',
              style: TextStyle(fontSize: 16),
            ),
          ),
          SizedBox(
            height: 60,
            child: Form(
              child: Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                child: TextFormField(
                    decoration: InputDecoration(
                        hintText: "Name",
                        filled: true,
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide.none))),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 20, top: 15),
                child: Text(
                  'Position Role',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Container(
                  width: 400,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey[200],
                  ),
                  child: Wrap(
                    spacing: 5,
                    children: _seleteRole
                        .map(
                          (e) => Padding(
                            padding: const EdgeInsets.only(top: 15, left: 10),
                            child: Text('$e,'),
                          ),
                        )
                        .toList(),
                  ),
                ),
              ),
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.grey[200],
                  ),
                  onPressed: _showPositionRole,
                  child: const Text(
                    'Select Role',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              const Divider(
                height: 30,
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: mainColor,
                fixedSize: const Size(100, 40),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PositionListScreen(),
                  ),
                );
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

class MultiSelect extends StatefulWidget {
  final List<String> items;

  const MultiSelect({Key? key, required this.items}) : super(key: key);

  @override
  State<MultiSelect> createState() => _MultiSelectState();
}

class _MultiSelectState extends State<MultiSelect> {
  final List<String> _selectedRole = [];

  void _roleChange(String roleValue, bool isSelected) {
    setState(() {
      if (isSelected) {
        _selectedRole.add(roleValue);
      } else {
        _selectedRole.remove(roleValue);
      }
    });
  }

  void _cancel() {
    Navigator.pop(context);
  }

  void _submit() {
    Navigator.pop(context, _selectedRole);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Select Role'),
      content: SingleChildScrollView(
        child: ListBody(
          children: widget.items
              .map((item) => CheckboxListTile(
                    value: _selectedRole.contains(item),
                    title: Text(item),
                    controlAffinity: ListTileControlAffinity.leading,
                    onChanged: (isSelected) => _roleChange(item, isSelected!),
                  ))
              .toList(),
        ),
      ),
      actions: [
        TextButton(
          onPressed: _cancel,
          child: const Text('Cancel'),
        ),
        ElevatedButton(
          onPressed: _submit,
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
