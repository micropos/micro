import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';

import 'package:micro_pos/pages/Position/position.dart';
import 'package:micro_pos/pages/Report/report.dart';

class PositionListScreen extends StatefulWidget {
  const PositionListScreen({super.key});

  @override
  State<PositionListScreen> createState() => _PositionListScreenState();
}

class _PositionListScreenState extends State<PositionListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff002D71),
        title: const Text('Position List'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.all(10.0),
            ),
             _getListTile("Kyaw Kyaw", "edit"),
             _getListTile("Kyaw Kyaw", "edit,delete"),
             _getListTile("Kyaw Kyaw", "edit,delete"),
             _getListTile("Kyaw Kyaw", "edit"),
             _getListTile("Kyaw Kyaw", "edit"),
             _getListTile("Kyaw Kyaw", "edit,delete"),
             _getListTile("Kyaw Kyaw", "edit"),
            
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>  PositionScreen(title: "Add Position",),
            ),
          );
        },
        backgroundColor: Color(0xff002D71),
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _getListTile(String n1, n2) {
    return Card(
      child: InkWell(
        onTap: () {},
        splashColor: Color.fromARGB(255, 92, 92, 92),
        child: ListTile(
            title: Text("$n1"),
            subtitle: Text("$n2"),
            trailing: Container(
              child: PopupMenuButton(
                child: Icon(Icons.more_vert),
                itemBuilder: (context) => [
                  PopupMenuItem(
                    child: Text("Edit"),
                    value: 'edit',
                  ),
                  PopupMenuItem(
                    child: Text("Delete"),
                    value: 'delete',
                  ),
                ],
                onSelected: (value) {
                  if (value == 'edit') {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => PositionScreen(
                              title: "Edit Position",
                            )));
                  } else {
                    if (Platform.isAndroid) {
                      showDialog(
                          context: context,
                          builder: (context) => simpleDialog(context));
                    } else {
                      showCupertinoDialog(
                          context: context,
                          builder: (context) => cupertinoDialog(context));
                    }
                  }
                },
              ),
            )),
      ),
    );
  }
  // Widget _getListTile() {
  //   return Card(
  //     child: InkWell(
  //       onTap: () {},
  //       splashColor: const Color.fromARGB(255, 92, 92, 92),
  //       child: const ListTile(
  //         title: Text("Kyaw Kyaw"),
  //         subtitle: Text("Edit, Delete"),
  //       ),
  //     ),
  //   );
  // }

  // Widget _getListTileOne() {
  //   return Card(
  //     child: InkWell(
  //       onTap: () {},
  //       splashColor: const Color.fromARGB(255, 92, 92, 92),
  //       child: const ListTile(
  //         title: Text("Su Su"),
  //         subtitle: Text("Edit"),
  //       ),
  //     ),
  //   );
  // }
}
