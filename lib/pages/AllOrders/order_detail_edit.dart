import 'package:flutter/material.dart';

import '../../Components/constants.dart';
import 'constant.dart';

class OrderDetailEdit extends StatefulWidget {
  const OrderDetailEdit({super.key});

  @override
  State<OrderDetailEdit> createState() => _OrderDetailEditState();
}

class _OrderDetailEditState extends State<OrderDetailEdit> {
  int _counter = 1;
  void increase() {
    setState(() {
      _counter++;
    });
  }

  void decrease() {
    if (_counter > 0) {
      setState(() {
        _counter--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainsColor,
        leading: GestureDetector(
            onTap: () => Navigator.pop(context), child: iosBackIcon),
        title: Text('Order Details'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textRow('Invoice Number :', 'INV27202316830002661'),
          textRow('Payment Method :', 'CASH'),
          textRow('Product Info :', '1.test2'),
          Container(
            margin: EdgeInsets.only(left: 20),
            //color: Colors.amber,
            width: 200,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Qty :',
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(
                  width: 60,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () => decrease(),
                      child: Text(
                        '-',
                        style: TextStyle(fontSize: 20, color: Colors.red),
                      ),
                    ),
                    SizedBox(width: 15,),
                    Text(
                      '$_counter',
                      style: TextStyle(fontSize: 20),
                    ),
                    SizedBox(width: 15,),
                    GestureDetector(
                      onTap: () => increase(),
                      child: Text(
                        '+',
                        style: TextStyle(fontSize: 20),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          textRow('Price :', '1000.0Ks'),
          Divider(
            color: Colors.black,
            thickness: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 20),
                child: const Text(
                  'PICK UP',
                  style: TextStyle(fontSize: 20),
                ),
              ),
              const Icon(
                Icons.keyboard_arrow_down,
                size: 40,
              ),
            ],
          ),
          Divider(
            color: Colors.black,
            thickness: 1,
          ),
          textRow('Sub Total :', '1000.0Ks', color: Colors.grey),
          textRow('Tax :', '0.00Ks', color: Colors.grey),
          textRow('Discount :', '0.00Ks', color: Colors.grey),
          textRow('Delivery Fee :', '0.00Ks', color: Colors.grey),
          Divider(
            color: Colors.black,
            thickness: 1,
          ),
          textRow('Total Price :', '1000.0Ks'),
          SizedBox(height: 60,),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              width: double.infinity,
              child: kAddButton("Confirm", (){})),
          )
        ],
      ),
    );
  }

  // Widget orderDetailButton(text, color) {
  //   return Container(
  //     margin: EdgeInsets.only(left: 30, top: 25),
  //     width: 350,
  //     height: 60,
  //     child: ElevatedButton(
  //       onPressed: () {},
  //       child: Text(
  //         text,
  //         style: TextStyle(fontSize: 17),
  //       ),
  //       style: ElevatedButton.styleFrom(
  //         backgroundColor: color,
  //       ),
  //     ),
  //   );
  // }

  Widget textRow(text1, text2, {color}) {
    return Container(
      margin: EdgeInsets.only(left: 20, top: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: TextStyle(fontSize: 18, color: color),
          ),
          Text(
            text2,
            style: TextStyle(fontSize: 18, color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
