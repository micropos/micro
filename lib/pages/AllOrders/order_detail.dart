import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';

import 'constant.dart';

class OrderDetail extends StatefulWidget {
  const OrderDetail({super.key});

  @override
  State<OrderDetail> createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainsColor,
        leading: GestureDetector(
            onTap: () => Navigator.pop(context), child: iosBackIcon),
        title: Text('Order Details'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          textRow('Invoice Number :', 'INV27202316830002661'),
          textRow('Payment Method :', 'CASH'),
          textRow('Product Info :', '1.test2'),
          textRow('Qty :', '1'),
          textRow('Price :', '1000.0Ks'),
          Divider(
            color: Colors.black,
            thickness: 1,
          ),
          textRow('Sub Total :', '1000.0Ks', color: Colors.grey),
          textRow('Tax :', '0.00Ks', color: Colors.grey),
          textRow('Discount :', '0.00Ks', color: Colors.grey),
          textRow('Delivery Fee :', '0.00Ks', color: Colors.grey),
          Divider(
            color: Colors.black,
            thickness: 1,
          ),
          textRow('Total Price :', '0.00Ks'),
          SizedBox(height: 80,),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              width: double.infinity,
              child: _kAddButton("PDF RECEIPT", (){
                
              }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              width: double.infinity,
              child: kAddButton("PRINT VIA THERMAL PRINTER", (){
                
              }),
            ),
          )
        ],
      ),
    );
  }

  CupertinoButton _kAddButton(String label, Function onTap) {
  return CupertinoButton(
      //minSize: double.infinity,
      color: Color(0xff009595),
      child: Text("$label"),
      onPressed: () => onTap());
}

  Widget textRow(text1, text2, {color}) {
    return Container(
      margin: EdgeInsets.only(left: 20, top: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text1,
            style: TextStyle(fontSize: 18, color: color),
          ),
          Text(
            text2,
            style: TextStyle(fontSize: 18, color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
