import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/pages/AllOrders/order_detail.dart';
import 'package:micro_pos/pages/AllOrders/order_detail_edit.dart';

import '../../Components/constants.dart';
import 'constant.dart';

class OrderHistory extends StatefulWidget {
  const OrderHistory({super.key});

  @override
  State<OrderHistory> createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  String? dropDownValue = 'Find By Order Status';
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _orderController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainsColor,
        centerTitle: false,
        title: Text('Order History'),
      ),
      body: Column(children: [
        Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
                controller: _orderController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 15),
                    suffixIcon: Icon(Icons.search),
                    hintText: "Search here...",
                    filled: true,
                    fillColor: textFieldColor,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide.none))),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          padding: const EdgeInsets.only(left: 10),
          width: 385,
          height: 40,
          decoration: BoxDecoration(
            color: textFieldColor,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Color(0xff002D71))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '$dropDownValue',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black
                ),
              ),
              DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  size: 28,
                  color: Color(0xff002d71),
                ),
                items: orderHistoryDropDownList
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                      value: value, child: Text(value));
                }).toList(),
                onChanged: (String? value) {
                  setState(() {
                    dropDownValue = value!;
                  });
                },
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => OrderDetail()));
          },
          child: dataContainer(
            'Mg Kyaw Kyaw',
            'May 03,2023  8:17AM',
          ),
        ),
        dataContainer(
          'Ma Hla Hla',
          'May 04,2023  9:00AM',
        )
      ]),
    );
  }

  Container dataContainer(text1, text2) {
    return Container(
      margin: const EdgeInsets.only(left: 20, top: 40, right: 10),
      padding: const EdgeInsets.only(left: 29, right: 10, top: 8),
      width: 400,
      height: 185,
      decoration: BoxDecoration(
          color: const Color(0xfff3f1f1),
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: const Color(0xff002D71))),
      child: Container(
        //color: Colors.green,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    orderRow('assets/allOrder1.png', text1),
                    orderRow('assets/allOrder2.png', 'INV27202316830002661'),
                    orderRow('assets/allOrder3.png', text2),
                    Container(
                      width: 200,
                      height: 33,
                      //color: Colors.amber,
                      child: Row(
                        children: [
                          Container(
                              width: 70,
                              height: 25,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  //color: Colors.white,
                                  border: Border.all()),
                              child: Text(
                                'PICK UP',
                                style: TextStyle(fontSize: 17),
                              )),
                          Container(
                              width: 50,
                              height: 25,
                              alignment: Alignment.center,
                              color: Color(0xff002D71),
                              child: Text(
                                'CASH',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 17),
                              )),
                        ],
                      ),
                    )
                  ],
                ),
                PopupMenuButton(
                  onSelected: (items) {
                    if (items == 'delete') {
                      showCupertinoDialog(
                          context: context,
                          builder: (context) => _deleteDialog());
                      // showDialog(
                      //     context: context,
                      //     builder: (context) {
                      //       return AlertDialog(
                      //         title: Center(child: Text('DELETE!')),
                      //         content: Text('Are you sure you want to delete?'),
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius: BorderRadius.circular(15)),
                      //         actions: [
                      //           Column(
                      //             children: [
                      //               Divider(
                      //                 thickness: 10,
                      //                 color: Colors.black,
                      //               ),
                      //               Row(
                      //                 mainAxisAlignment:
                      //                     MainAxisAlignment.spaceAround,
                      //                 children: [
                      //                   ElevatedButton(
                      //                       onPressed: () {},
                      //                       child: Text('Ok'),
                      //                       style: ElevatedButton.styleFrom(
                      //                           backgroundColor:
                      //                               Color(0xff002D71))),
                      //                   VerticalDivider(
                      //                     thickness: 5,
                      //                     color: Colors.black,
                      //                   ),
                      //                   ElevatedButton(
                      //                       onPressed: () {
                      //                         Navigator.pop(context);
                      //                       },
                      //                       child: Text('Cancel'),
                      //                       style: ElevatedButton.styleFrom(
                      //                           backgroundColor:
                      //                               Color(0xff002D71)))
                      //                 ],
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       );
                      //     });
                      // // CupertinoAlertDialog(
                      // //   title: Center(child: Text('DELETE!')),
                      // //   content: Text('Are you sure you want to delete?'),
                      // //   actions: [
                      // //     CupertinoDialogAction(
                      // //         child: ElevatedButton(
                      // //       onPressed: () {},
                      // //       child: Text('Ok'),
                      // //     )),
                      // //     CupertinoDialogAction(
                      // //         child: ElevatedButton(
                      // //       onPressed: () {},
                      // //       child: Text('Cancel'),
                      // //     ))
                      // //   ],
                      // // );
                    } else if (items == 'edit') {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => OrderDetailEdit()));
                    }
                  },
                  icon: Image.asset('assets/allOrder4.png'),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: 'delete',
                      child: Text('DELETE'),
                    ),
                    const PopupMenuItem(
                      value: 'edit',
                      child: Text('EDIT'),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget orderRow(image, text) {
    return Container(
      //color: Colors.green,
      width: 250,
      height: 45,
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: 33,
              height: 50,
              //color: Colors.red,
              child: Image.asset(image)),
          SizedBox(
            width: 10,
          ),
          Container(
            width: 190,
            height: 50,
            padding: EdgeInsets.only(top: 15),
            //color: Colors.red,
            child: Text(
              text,
              style: TextStyle(fontSize: 15),
            ),
          )
        ],
      ),
    );
  }

  CupertinoAlertDialog _deleteDialog() {
    return CupertinoAlertDialog(
      title: Text(
        "Delete!",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Are you sure you want to delete"),
      actions: [
        CupertinoDialogAction(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        CupertinoDialogAction(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
