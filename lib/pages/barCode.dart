// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
// import 'package:micro_pos/Components/constants.dart';

// class BarCode extends StatefulWidget {
//   const BarCode({super.key});

//   @override
//   State<BarCode> createState() => _BarCodeState();
// }

// class _BarCodeState extends State<BarCode> {
//   String data = '';
//   _scan() async {
//     return await FlutterBarcodeScanner.scanBarcode(
//             "#000000", "Cancel", true, ScanMode.BARCODE)
//         .then((value) {
//       setState(() {
//         data == value;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("BarCode Scanner"),
//         centerTitle: true,
//         backgroundColor: mainColor,
//       ),
//       body: Column(
//         children: [
//           TextButton(
//             style: TextButton.styleFrom(
//               backgroundColor: Colors.black
//             ),
//               onPressed: () => _scan(),
//               child: Text(data))
//         ],
//       ),
//     );
//   }
// }
