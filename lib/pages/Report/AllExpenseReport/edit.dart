import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';

import 'allexpense.dart';

class ExpenseEdit extends StatefulWidget {
  const ExpenseEdit({super.key});

  @override
  State<ExpenseEdit> createState() => _ExpenseEditState();
}

class _ExpenseEditState extends State<ExpenseEdit> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _customerNameController = TextEditingController();
  TextEditingController _customerPhoneController = TextEditingController();
  TextEditingController _customerEmailController = TextEditingController();
  TextEditingController _customerAddressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Edit Expense"),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => AllExpense()));
              },
              child: Text(
                "Done",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ))
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        "Expense Name",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      validator: (value) => value!.isEmpty
                          ? "Customer Name can't be empty"
                          : null,
                      controller: _customerNameController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(20)),
                          hintText: "expense name",
                          //helperText: "Customer Name",
                          helperStyle: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Expense Note(if any)",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    TextFormField(
                      validator: (value) =>
                          value!.isEmpty ? "Address can't be empty" : null,
                      controller: _customerAddressController,
                      keyboardType: TextInputType.multiline,
                      maxLines: 6,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(20)),
                          hintText: "test",
                          //helperText: "Customer Name",
                          helperStyle: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        "Expense Amount",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      validator: (value) =>
                          value!.isEmpty ? "Email can't be empty" : null,
                      controller: _customerEmailController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(20)),
                          hintText: "1000.0ks",
                          //helperText: "Customer Name",
                          helperStyle: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        "Expense Date",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      validator: (value) =>
                          value!.isEmpty ? "Email can't be empty" : null,
                      controller: _customerEmailController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(20)),
                          hintText: "2023-05-02",
                          //helperText: "Customer Name",
                          helperStyle: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        "Expense Time",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      validator: (value) =>
                          value!.isEmpty ? "Email can't be empty" : null,
                      controller: _customerEmailController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: textFieldColor,
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(20)),
                          hintText: "01:00PM",
                          //helperText: "Customer Name",
                          helperStyle: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                  ]),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: SizedBox(
                width: double.infinity,
                child: kAddButton("Edit", () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => AllExpense()));
                  }
                })),
          )
        ],
      ),
    );
  }
}
