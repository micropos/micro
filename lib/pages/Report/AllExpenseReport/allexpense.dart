import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/pages/Report/AllExpenseReport/weekexp.dart';
import 'package:micro_pos/pages/Report/AllExpenseReport/yearexp.dart';
import 'package:micro_pos/pages/Report/report.dart';




import 'daiexp.dart';
import 'edit.dart';
import 'monthexp.dart';

class AllExpense extends StatefulWidget {
  const AllExpense({super.key});

  @override
  State<AllExpense> createState() => _AllExpenseState();
}

class _AllExpenseState extends State<AllExpense> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("All expense report"),
        actions: [
        getPopUp1(),
        ],
      ),
      body: Column(
        children: [
           Form(
            
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                  
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 15),
                      suffixIcon: Icon(Icons.search),
                      hintText: "Search here...",
                      filled: true,
                      
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none))),
            ),
          ),
          Container(
            
            color: Colors.white,
            width: 350,
            height: 120,
            child: Row(
              children: [
                Column(
                  
                  children: [
                    Image.asset('assets/report6.jpg',height: 120,),
                  ],
                ),
                Container(
                  width: 150,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 5,),
                      Text("Watch",style: TextStyle(fontSize: 24,color: mainColor),),
                      SizedBox(height: 5,),
                      Text("expense:     1000ks"),
                      SizedBox(height: 5,),
                      Text("2023-04-28   10:38PM"),
                      SizedBox(height: 5,),
                      Text("Note: test"),
                      
                    ],
                    
                  ),
                  
                ),
                Column(
                  children: [
                    SizedBox(height: 5,),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFFD9D9D9),
                        shape: CircleBorder()
                      ),
                      onPressed: (){
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Center(child: Text('DELETE!')),
                              content: Text('Are you sure you want to delete?'),
                              actions: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {},
                                        child: Text('Ok'),
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor:
                                                Color(0xff002D71))),
                                    ElevatedButton(
                                        onPressed: () {},
                                        child: Text('Cancel'),
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Color(0xff002D71)))
                                  ],
                                )
                              ],
                            );
                          });

                    }, child: Icon(Icons.delete,color: Colors.red,
                    ),),
                    SizedBox(height: 15,),
                    ElevatedButton(
                       style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFFD9D9D9),
                        shape: CircleBorder()
                      ),
                      onPressed: (){
                         Navigator.push(context, MaterialPageRoute(builder: (context)=>ExpenseEdit()));
                    }, child: Icon(Icons.edit,color: mainColor,
                    ),),
                  ],
                ),
                
              ],
            ),
          ),
          SizedBox(height: 40,),
          Divider(
            thickness: 2,
          ),
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Expense:"),
                Text("1000.0ks"),
              ],
            ),
          )
        ],
      ),
    );
  }
  Widget getPopUp1(){
    return PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'aLL',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'dAY',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wEK',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mON',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'yEAR',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'aLL'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllExpense()));
            }else if(value =='dAY'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DaiExp()));
            }
            else if(value =='wEEK'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>WeekExp()));
            }else if(value =='mON'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MonExp()));
            }else if(value =='yEAR'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>YearExp()));
            }
          },

        );
  }
}