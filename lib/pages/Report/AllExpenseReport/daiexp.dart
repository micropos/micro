import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/pages/Report/AllExpenseReport/weekexp.dart';
import 'package:micro_pos/pages/Report/AllExpenseReport/yearexp.dart';
import 'package:micro_pos/pages/Report/report.dart';



import 'allexpense.dart';
import 'monthexp.dart';

class DaiExp extends StatefulWidget {
  const DaiExp({super.key});

  @override
  State<DaiExp> createState() => _DaiExpState();
}

class _DaiExpState extends State<DaiExp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Daily expense report"),
        actions: [
        getPopUp1(),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30,),
          Text("Total expense:"),
          SizedBox(
            height: 30,
          ),
          Center(child: Image.asset('assets/report5.png',width: 80,height: 80,)),
        ],
      ),
    );
  }
  Widget getPopUp1(){
    return PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'aLL',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'dAY',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wEK',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mON',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'yEAR',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'aLL'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllExpense()));
            }else if(value =='dAY'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DaiExp()));
            }
            else if(value =='wEEK'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>WeekExp()));
            }else if(value =='mON'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MonExp()));
            }else if(value =='yEAR'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>YearExp()));
            }
          },

        );
  }
}