import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/pages/Report/AllExpenseReport/yearexp.dart';


import 'allexpense.dart';
import 'daiexp.dart';
import 'monthexp.dart';


class WeekExp extends StatefulWidget {
  const WeekExp({super.key});

  @override
  State<WeekExp> createState() => _WeekExpState();
}

class _WeekExpState extends State<WeekExp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text("Weekly expense report"),
        actions: [
        getPopUp1(),
        ],
      ),
    );
  }
  Widget getPopUp1(){
    return PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'aLL',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'dAY',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wEK',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mON',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'yEAR',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'aLL'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllExpense()));
            }else if(value =='dAY'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DaiExp()));
            }
            else if(value =='wEEK'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>WeekExp()));
            }else if(value =='mON'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MonExp()));
            }else if(value =='yEAR'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>YearExp()));
            }
          },

        );
  }
}