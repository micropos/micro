import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/allsale.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/daily.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/monthly.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/weekly.dart';
import 'package:micro_pos/pages/Report/report.dart';




class Yearly extends StatefulWidget {
  const Yearly({super.key});

  @override
  State<Yearly> createState() => _YearlyState();
}

class _YearlyState extends State<Yearly> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Yearly"),
        actions: [
          PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'all',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'day',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wek',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mon',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'year',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'all'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllSale()));
            }else if(value =='day'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Daily()));
            }
            else if(value =='wek'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Weekly()));
            }else if(value =='mon'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Monthly()));
            }else if(value =='year'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Yearly()));
            }
          },

        )
        ],
      ),
       body: SingleChildScrollView(
         child: Column(
           children: [
             Table(
              border: TableBorder.all(width: 1,color: Colors.black),
              children: [
                _tableRow("No", "Name","Qty", "Prices"),
                _tableRow("1", "text1","1" ,"100.0ks"),
                _tableRow("2", "text2","1" ,"150.0ks"),
                _tableRow("3", "text3","1" ,"200.0ks"),
                _tableRow("4", "text4","1" ,"100.0ks"),
                _tableRow("5", "text1","1" ,"100.0ks"),
                _tableRow("6", "text2","1" ,"150.0ks"),
                _tableRow("7", "text3","1" ,"200.0ks"),
                _tableRow("8", "text4","1" ,"100.0ks"),
                _tableRow("9", "text1","1" ,"100.0ks"),
                _tableRow("10", "text2","1" ,"150.0ks"),
                _tableRow("11", "text3","1" ,"200.0ks"),
                _tableRow("12", "text4","1" ,"100.0ks"),
                _tableRow("13", "text1","1" ,"100.0ks"),
                _tableRow("14", "text2","1" ,"150.0ks"),
                _tableRow("15", "text3","1" ,"200.0ks"),
                _tableRow("16", "text4","1" ,"100.0ks"),
              ],
             ),
             SizedBox(height: 20,),
            Container(
                    child: Padding(
                      padding: const EdgeInsets.all(30),
                      child: Column(
                        children: [
                          
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Expense:"),
                              Text("120,000")
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Tax:"),
                              Text("500")
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Discount:"),
                              Text("2,000")
                            ],
                          ),
                          
                          
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Net Sale:"),
                              Text("118,500")
                            ],
                          ),
                          
                        ],
                      ),
                      
                    ),
                  ),
           ],
         ),
       ),
      
    );
  }
}
TableRow _tableRow(t1,t2,t3,t4){
   return  TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Center(child: Text(t1),),
        ),
         Padding(
           padding: const EdgeInsets.all(5.0),
           child: Center(child: Text(t2),),
         ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t3),),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t4),),
          ),
      ]
    );
}