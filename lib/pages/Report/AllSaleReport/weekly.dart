import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/allsale.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/daily.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/monthly.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/yearly.dart';
import 'package:micro_pos/pages/Report/report.dart';





class Weekly extends StatefulWidget {
  const Weekly({super.key});

  @override
  State<Weekly> createState() => _WeeklyState();
}

class _WeeklyState extends State<Weekly> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Weekly"),
        actions: [
          PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'all',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'day',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wek',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mon',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'year',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'all'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllSale()));
            }else if(value =='day'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Daily()));
            }
            else if(value =='wek'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Weekly()));
            }else if(value =='mon'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Monthly()));
            }else if(value =='year'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Yearly()));
            }
          },

        )
        ],
      ),
       body: Column(
        children: [
          Table(
            border: TableBorder.all(width: 1,color: Colors.black),
            children: [
              _tableRow("No", "Name","Qty", "Prices"),
              _tableRow("1", "text1","1" ,"100.0ks"),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            
            width: 300,
            height: 100,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total Price:"),
                    Text("100.0ks"),
                    
                  ],
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total Tax:"),
                    Text("0.0ks"),
                    
                  ],
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total Discount:"),
                    Text("0.0ks"),
                    
                  ],
                ),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Net Price:"),
                    Text("100.0ks"),
                    
                  ],
                ),
              ],
            ),
            
          )
        ],
      ),
    );
  }
}
TableRow _tableRow(t1,t2,t3,t4){
   return  TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Center(child: Text(t1),),
        ),
         Padding(
           padding: const EdgeInsets.all(5.0),
           child: Center(child: Text(t2),),
         ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t3),),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t4),),
          ),
      ]
    );
}