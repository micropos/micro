

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/daily.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/monthly.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/weekly.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/yearly.dart';
import 'package:micro_pos/pages/Report/report.dart';




class AllSale extends StatefulWidget {
  const AllSale({super.key});

  @override
  State<AllSale> createState() => _AllSaleState();
}

class _AllSaleState extends State<AllSale> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text('All Sale'),
        actions: [
        getPopUp(),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Table(
              border: TableBorder.all(width: 1,color: Colors.black),
              children: [
                 _tableRow("No", "Name","Qty", "Prices"),
                _tableRow("1", "text1","1" ,"100.0ks"),
                _tableRow("2", "text2","1" ,"150.0ks"),
                _tableRow("3", "text3","1" ,"200.0ks"),
                _tableRow("4", "text4","1" ,"100.0ks"),
                _tableRow("5", "text1","1" ,"100.0ks"),
                _tableRow("6", "text2","1" ,"150.0ks"),
                _tableRow("7", "text3","1" ,"200.0ks"),
                _tableRow("8", "text4","1" ,"100.0ks"),
                _tableRow("9", "text1","1" ,"100.0ks"),
                _tableRow("10", "text2","1" ,"150.0ks"),
                _tableRow("11", "text3","1" ,"200.0ks"),
                _tableRow("12", "text4","1" ,"100.0ks"),
                _tableRow("13", "text1","1" ,"100.0ks"),
                _tableRow("14", "text2","1" ,"150.0ks"),
                _tableRow("15", "text3","1" ,"200.0ks"),
                _tableRow("16", "text4","1" ,"100.0ks"),
                _tableRow("17", "text1","1" ,"100.0ks"),
                _tableRow("18", "text2","1" ,"150.0ks"),
                _tableRow("19", "text3","1" ,"200.0ks"),
                _tableRow("20", "text4","1" ,"100.0ks"),
                _tableRow("21", "text1","1" ,"100.0ks"),
                _tableRow("22", "text2","1" ,"150.0ks"),
                _tableRow("23", "text3","1" ,"200.0ks"),
                _tableRow("24", "text4","1" ,"100.0ks"),
                _tableRow("25", "text1","1" ,"100.0ks"),
                _tableRow("26", "text2","1" ,"150.0ks"),
                _tableRow("27", "text3","1" ,"200.0ks"),
                _tableRow("28", "text4","1" ,"100.0ks"),
                _tableRow("29", "text1","1" ,"100.0ks"),
                _tableRow("30", "text2","1" ,"150.0ks"),
                _tableRow("31", "text3","1" ,"200.0ks"),
                _tableRow("32", "text4","1" ,"100.0ks"),
      
      
              ],
            ),
            SizedBox(height: 20,),
            Container(
                    child: Padding(
                      padding: const EdgeInsets.all(30),
                      child: Column(
                        children: [
                          
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Expense:"),
                              Text("275,000")
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Tax:"),
                              Text("1,000")
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Total Discount:"),
                              Text("3,000")
                            ],
                          ),
                          
                          
                          SizedBox(height: 10,),
                          Row(
                            
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Net Sale:"),
                              Text("273,000")
                            ],
                          ),
                          
                        ],
                      ),
                      
                    ),
                  ),
          ],
        ),
      ),
      
    );
  }
   Widget getPopUp(){
    return PopupMenuButton(
          child: Icon(Icons.calendar_month),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text("All"),
              value: 'all',
            ),
             PopupMenuItem(
              child: Text("Daily"),
              value: 'day',
            ),
            PopupMenuItem(
              child: Text("Weekly"),
              value: 'wek',
            ),
            PopupMenuItem(
              child: Text("Monthly"),
              value: 'mon',
            ),
            PopupMenuItem(
              child: Text("Yearly"),
              value: 'year',
            ),
            
          ],
          onSelected: (value) {
            if(value == 'all'){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AllSale()));
            }else if(value =='day'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Daily()));
            }
            else if(value =='wek'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Weekly()));
            }else if(value =='mon'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Monthly()));
            }else if(value =='year'){
               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Yearly()));
            }
          },

        );
  }
  TableRow _tableRow(t1,t2,t3,t4){
   return  TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Center(child: Text(t1),),
        ),
         Padding(
           padding: const EdgeInsets.all(5.0),
           child: Center(child: Text(t2),),
         ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t3),),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Center(child: Text(t4),),
          ),
      ]
    );
}

}