// import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:micro_pos/Report/report.dart';


// import 'package:syncfusion_flutter_charts/charts.dart';

// class ExpenseGraph extends StatefulWidget {
//   const ExpenseGraph({super.key});

//   @override
//   State<ExpenseGraph> createState() => _ExpenseGraphState();
// }

// class _ExpenseGraphState extends State<ExpenseGraph> {

//   final year=['2020','2021','2022','2023','2024','2025','2026','2027'];
//   var selectedValue = "2023";
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: false,
//         backgroundColor: mainColor,
//         title: Text("Expense Graph"),
//       ),
//       body:
//           SingleChildScrollView(
//             child: Column(
//               children: [
//                 SizedBox(height: 15,),
                
                
//                 Container(
//                   width: 100,
//                   child: DropdownButtonHideUnderline(
//                     child: DropdownButton<String>(
//                       hint: Text("$selectedValue"),
//                       items: year.map(yearMenu).toList(),
//                       onChanged:(value)=> setState(() {
//                         selectedValue = value!;
//                       })
//                       ),
//                   ),
//                 ),
//                 Container(
//                   height: 450,
//                   child: SfCartesianChart(
                    
//                     primaryXAxis: CategoryAxis(title: AxisTitle(text: "")),
//                     primaryYAxis: NumericAxis(title: AxisTitle(text: "")),
//                     legend: Legend(isVisible: true),
//                     series: <ChartSeries>[
//                       ColumnSeries<SalesData, String>( //LineSeries//ColumnSeries
//                           name: "$selectedValue",
//                           dataSource: getColumnData(),
//                           xValueMapper: (SalesData sales, _) => sales.x,
//                           yValueMapper: (SalesData sales, _) => sales.y,
//                           dataLabelSettings: DataLabelSettings(isVisible: true)),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   child: Padding(
//                     padding: const EdgeInsets.all(30),
//                     child: Column(
//                       children: [
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("January:"),
//                             Text("10,000")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("February:"),
//                             Text("5,000")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("March:"),
//                             Text("1,000")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("April:"),
//                             Text("6,000")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("May:"),
//                             Text("4,000")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("June:"),
//                             Text("0")
//                           ],
//                         ),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("July:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("August:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("September:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("October:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("Novenber:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("December:"),
//                             Text("0")
//                           ],
//                         ),
//                         SizedBox(height: 15,),
//                         Divider(
//                           thickness: 2,
//                         ),
//                         SizedBox(height: 10,),
//                         Row(
                          
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Text("Total Expense:"),
//                             Text("26,000")
//                           ],
//                         ),
                        
//                       ],
//                     ),
                    
//                   ),
//                 ),
//               ],
//             ),
//           ),

       
//     );
//   }
//   DropdownMenuItem<String>yearMenu(String year)=>DropdownMenuItem(
//     value: year,
//     child: Text(year));
// }
// class SalesData {
//   String x;
//   double y;

//   SalesData(this.x, this.y);
// }
// dynamic getColumnData() {
//   List<SalesData> columnData = [
//     SalesData("Jan", 10000),
//     SalesData("Feb", 5000),
//     SalesData("Mar", 1000),
//     SalesData("April", 6000),
//     SalesData("May", 4000),
//     SalesData("June", 0),
//     SalesData("July", 0),
//     SalesData("Aug", 0),
//     SalesData("Sep", 0),
//     SalesData("Oct", 0),
//     SalesData("Nov", 0),
//     SalesData("Dec",0),
//   ];

//   return columnData;
// }

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:micro_pos/pages/Report/report.dart';

import 'package:syncfusion_flutter_charts/charts.dart';

class ExpenseGraph extends StatefulWidget {
  const ExpenseGraph({super.key});

  @override
  State<ExpenseGraph> createState() => _ExpenseGraphState();
}

class _ExpenseGraphState extends State<ExpenseGraph> {
  final year = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027'];
  var selectedValue = "2023";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Expense Graph"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            Container(
              width: 100,
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                    hint: Text(
                      "$selectedValue",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    items: year.map(yearMenu).toList(),
                    onChanged: (value) => setState(() {
                          selectedValue = value!;
                        })),
              ),
            ),
            Container(
              height: 450,
              child: SfCartesianChart(
                primaryXAxis: CategoryAxis(title: AxisTitle(text: "")),
                primaryYAxis: NumericAxis(title: AxisTitle(text: "")),
                legend: Legend(isVisible: true),
                series: <ChartSeries>[
                  ColumnSeries<SalesData, String>(
                      //LineSeries//ColumnSeries
                      name: "$selectedValue",
                      dataSource: selectedValue == '2022' ? getColumnData2() : getColumnData(),
                      xValueMapper: (SalesData sales, _) => sales.x,
                      yValueMapper: (SalesData sales, _) => sales.y,
                      dataLabelSettings: DataLabelSettings(isVisible: true)),
                ],
              ),
            ),
            // Container(
            //   child: Padding(
            //     padding: const EdgeInsets.all(30),
            //     child: Column(
            //       children: [
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("January:"), Text("10,000")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("February:"), Text("5,000")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("March:"), Text("1,000")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("April:"), Text("6,000")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("May:"), Text("4,000")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("June:"), Text("0")],
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("July:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("August:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("September:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("October:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("Novenber:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("December:"), Text("0")],
            //         ),
            //         SizedBox(
            //           height: 15,
            //         ),
            //         Divider(
            //           thickness: 2,
            //         ),
            //         SizedBox(
            //           height: 10,
            //         ),
            //         Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [Text("Total Sale:"), Text("26,000")],
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
            selectedValue == '2022'
                ? _getMonthData(
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December',
                    '1,000',
                    '5,000',
                    '1,000',
                    '6,000',
                    '4,000',
                    '2,000',
                    '5,000',
                    '6,000',
                    '9,000',
                    '10,000',
                    '90,000',
                    '2,000',
                    '141,000')
                : _getMonthData(
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December',
                    '1,000',
                    '5,000',
                    '1,000',
                    '6,000',
                    '4,000',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '60,000'),
            //  _getMonthData('January','February','March','April','May','June','July','August','September','October','November','December','1,000',
            // '5,000','1,000','6,000','4,000','2,000','5,000','6,000','9,000','10,000','90,000','2,000','60,000'
            // ),
          ],
        ),
      ),
    );
  }

  Widget _getMonthData(
      String jan,
      feb,
      mar,
      april,
      may,
      jun,
      july,
      aug,
      sep,
      oct,
      nov,
      dec,
      String janP,
      febP,
      marP,
      aprilP,
      mayP,
      juneP,
      julyP,
      augP,
      septP,
      octP,
      novP,
      decP,
      total) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(30),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$jan:"), Text("$janP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$feb:"), Text("$febP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$mar:"), Text("$marP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$april:"), Text("$aprilP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$may:"), Text("$mayP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$jun:"), Text("$juneP")],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$july:"), Text("$julyP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$aug:"), Text("$augP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$sep:"), Text("$septP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$oct:"), Text("$octP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$nov:"), Text("$novP")],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("$dec:"), Text("$decP")],
            ),
            SizedBox(
              height: 15,
            ),
            Divider(
              thickness: 2,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Total Sale:"), Text("$total")],
            ),
          ],
        ),
      ),
    );
  }
  



  DropdownMenuItem<String> yearMenu(String year) =>
      DropdownMenuItem(value: year, child: Text(year));
}

class SalesData {
  String x;
  double y;

  SalesData(this.x, this.y);
}

dynamic getColumnData() {
  List<SalesData> columnData = [
    SalesData("Jan", 10000),
    SalesData("Feb", 5000),
    SalesData("Mar", 1000),
    SalesData("April", 6000),
    SalesData("May", 4000),
    SalesData("June", 0),
    SalesData("July", 0),
    SalesData("Aug", 0),
    SalesData("Sep", 0),
    SalesData("Oct", 0),
    SalesData("Nov", 0),
    SalesData("Dec", 0),
  ];

  return columnData;
}

dynamic getColumnData2() {
  List<SalesData> columnData = [
    SalesData("Jan", 10000),
    SalesData("Feb", 5000),
    SalesData("Mar", 1000),
    SalesData("April", 3000),
    SalesData("May", 4000),
    SalesData("June", 4000),
    SalesData("July", 5000),
    SalesData("Aug", 7000),
    SalesData("Sep", 8000),
    SalesData("Oct", 1000),
    SalesData("Nov", 9000),
    SalesData("Dec", 10000),
  ];

  return columnData;
}

