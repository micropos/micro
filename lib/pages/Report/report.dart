import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/allsale.dart';
import 'package:micro_pos/pages/Report/SaleGraph/salegraph.dart';




import 'AllExpenseReport/allexpense.dart';
import 'ExpenseGraph/expense.dart';

const mainColor=Color(0xff002D71);
const textFieldColor=Color.fromARGB(34, 158, 158, 158);
const buttonColor=Color.fromARGB(255, 198, 222, 254);
const buttonTextColor=Color(0xff0A82FF);

class Report extends StatefulWidget {
  const Report({super.key});

  @override
  State<Report> createState() => _ReportState();
}

class _ReportState extends State<Report> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text('Report',
        style: TextStyle(color: Colors.white),),
      ),
      body: Column(
        children: [
         InkWell(onTap: () {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>AllSale()));
         },
           child: Container(
            color: Colors.white,
            height: 80,
            child: Card(
              child: Row(
                children: [
                  SizedBox(width: 10,),
                  Image.asset('assets/report1.png',width: 60,height: 60,),
                  SizedBox(width: 20,),
                  Text('All Sale Report',
                  style:TextStyle(
                    fontSize: 20,
                  ) ,),
                ],
              ),
            ),
           ),
         ),
          InkWell(onTap: () {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>AllExpense()));
         },
           child: Container(
            height: 80,
            child: Card(
              child: Row(
                children: [
                  SizedBox(width: 10,),
                  Image.asset('assets/report2.png',width: 60,height: 60,),
                  SizedBox(width: 20,),
                  Text('All Expense Report',
                  style:TextStyle(
                    fontSize: 20,
                  ) ,),
                ],
              ),
            ),
           ),
         ),
          InkWell(onTap: () {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>SaleGraph()));
         },
           child: Container(
            height: 80,
            child: Card(
              child: Row(
                children: [
                  SizedBox(width: 10,),
                  Image.asset('assets/report3.png',width: 60,height: 60,),
                  SizedBox(width: 20,),
                  Text('Sale Graph',
                  style:TextStyle(
                    fontSize: 20,
                  ) ,),
                ],
              ),
            ),
           ),
         ),
          InkWell(onTap: () {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>ExpenseGraph()));
         },
           child: Container(
            height: 80,
            child: Card(
              child: Row(
                children: [
                  SizedBox(width: 10,),
                  Image.asset('assets/report4.png',width: 60,height: 60,),
                  SizedBox(width: 20,),
                  Text('Expense Graph',
                  style:TextStyle(
                    fontSize: 20,
                  ) ,),
                ],
              ),
            ),
           ),
         ),
        ],
      ),
    );
  }
}