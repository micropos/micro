import 'package:flutter/material.dart';

class Response extends StatelessWidget {
  // const Response({super.key});
  final Widget largeScreen;
  final Widget smallScreen;
  final Widget mediumScreen;

  Response(
      {Key? key,
      required this.largeScreen,
      required this.smallScreen,
      required this.mediumScreen})
      : super(key: key);

  static bool isSmallScreen(BuildContext context) {
    return MediaQuery.of(context).size.height < 800;
  }

  static bool isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.height >= 800 &&
        MediaQuery.of(context).size.height <= 1200;
  }

  static bool isLargeScreen(BuildContext context) {
    return MediaQuery.of(context).size.height > 1200;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      if (constraint.maxHeight > 1200) {
        return largeScreen;
      } else if (constraint.maxHeight <= 1200 && constraint.maxHeight >= 800) {
        return mediumScreen ?? largeScreen;
      } else {
        return smallScreen ?? largeScreen;
      }
    });
  }
}
