import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:micro_pos/pages/Coupon/couponList.dart';
import 'package:micro_pos/pages/Coupon/coupon_screen.dart';
import 'package:micro_pos/pages/Expense/expense.dart';
import 'package:micro_pos/pages/Position/position_list_screen.dart';
import 'package:micro_pos/pages/PreOrder/PreHome.dart';
import 'package:micro_pos/pages/Report/AllSaleReport/allsale.dart';
import 'package:micro_pos/pages/Report/report.dart';
import 'package:micro_pos/pages/Staff/allStaff.dart';
import 'package:micro_pos/pages/AllOrders/order_history.dart';
import 'package:micro_pos/pages/Customer/allCustomer.dart';
import 'package:micro_pos/pages/DeliveryFee/deliveryfee.dart';
import 'package:micro_pos/pages/Languages/language.dart';
import 'package:micro_pos/pages/POS/PosPage/pos_screen.dart';
import 'package:micro_pos/pages/Product/AllProduct.dart';
import 'package:micro_pos/pages/Product/FkScreen.dart';
import 'package:micro_pos/pages/Settings/settings.dart';
import 'package:micro_pos/pages/Suppliers/allSuppliers.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:auto_size_text/auto_size_text.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    final dataMap = <String, double>{
      "Flutter": 50,
    };
    var screenWidth = MediaQuery.of(context).size.width;
    final colorList = <Color>[
      Colors.greenAccent,
    ];

    final List<ChartData> chartData = [
      ChartData('+', 10),
      ChartData('+', 9),
      ChartData('+', 10),
    ];

    final List<ChartData> chartData2 = [
      ChartData('-', 11),
      ChartData('-', 12),
      ChartData('-', 8),
    ];

    return Scaffold(
      body: Container(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Positioned(
                left: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.20,
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)),
                    color: Color.fromARGB(255, 0, 60, 109),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.05,
                        left: 30),
                    child: const Text(
                      'MICRO POS',
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.10,
                left: 20,
                right: 20,
                child: Opacity(
                    opacity: 1,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color.fromARGB(255, 236, 236, 236),
                      ),
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      'assets/shop_copy.png',
                                      width: 30,
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width -
                                          110,
                                      child: const AutoSizeText(
                                        '- Micro Shop',
                                        style: TextStyle(fontSize: 16),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 5)),
                                Row(
                                  children: [
                                    Image.asset(
                                      'assets/call.png',
                                      width: 30,
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width -
                                          110,
                                      child: const AutoSizeText(
                                        '- 09776444356',
                                        style: TextStyle(fontSize: 16),
                                        maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 5)),
                                Container(
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/lacation.png',
                                        width: 30,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                110,
                                        child: const AutoSizeText(
                                          '- No.3 (3B), Adin Housing, Thamine Junction, Mayangone, Yangon.',
                                          style: TextStyle(fontSize: 16),
                                          maxLines: 4,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )),
              ),
              Positioned(
                top: -53,
                left: MediaQuery.of(context).size.width - 200,
                child: Opacity(
                    opacity: 1, //from 0-1, 0.5 = 50% opacity
                    child: Container(
                      height: 250,
                      width: 200,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: const DecorationImage(
                          image: AssetImage('assets/firstLogo.png'),
                        ),
                      ),
                    )),
              ),
              Positioned(
                  top: MediaQuery.of(context).size.height * 0.30,
                  left: 22,
                  child: Container(
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.40,
                              height: 72,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color.fromARGB(255, 236, 236, 236),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                      padding: EdgeInsets.only(
                                          left: 10, bottom: 10)),
                                  Row(
                                    children: [
                                      const Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, bottom: 10)),
                                      Image.asset(
                                        'assets/increasing.png',
                                        width: 20,
                                        color: Colors.blue,
                                      ),
                                      const Padding(
                                          padding: EdgeInsets.only(left: 10)),
                                      Text('Today Sales')
                                    ],
                                  ),
                                  Padding(padding: EdgeInsets.all(5)),
                                  Row(
                                    children: const [
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, bottom: 10)),
                                      Text(
                                        'Ks',
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                Color.fromARGB(255, 4, 0, 85)),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(left: 10)),
                                      Text('257,800',
                                          style: TextStyle(fontSize: 18))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 5)),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.40,
                              height: 73,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Color.fromARGB(255, 236, 236, 236),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                      padding: EdgeInsets.only(
                                          left: 10, bottom: 10)),
                                  Row(
                                    children: [
                                      const Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, bottom: 10)),
                                      Image.asset(
                                        'assets/package.png',
                                        width: 20,
                                        color: Colors.blue,
                                      ),
                                      const Padding(
                                          padding: EdgeInsets.only(left: 10)),
                                      Text('Today Orders')
                                    ],
                                  ),
                                  Padding(padding: EdgeInsets.all(5)),
                                  Row(
                                    children: const [
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, bottom: 10)),
                                      // Text(
                                      //   'P',
                                      //   style: TextStyle(
                                      //       fontSize: 18,
                                      //       fontWeight: FontWeight.bold,
                                      //       color:
                                      //           Color.fromARGB(255, 4, 0, 85)),
                                      // ),
                                      Padding(
                                          padding: EdgeInsets.only(left: 30)),
                                      Text(
                                        '72',
                                        style: TextStyle(fontSize: 18),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                          left: 10,
                        )),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          height: 150,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color.fromARGB(255, 236, 236, 236),
                          ),
                          child: SfCartesianChart(
                              primaryXAxis: CategoryAxis(arrangeByIndex: true),
                              series: <ChartSeries<ChartData, String>>[
                                ColumnSeries<ChartData, String>(
                                  dataSource: chartData,
                                  xValueMapper: (ChartData data, _) => data.x,
                                  yValueMapper: (ChartData data, _) => data.y,
                                ),
                                ColumnSeries<ChartData, String>(
                                  dataSource: chartData2,
                                  xValueMapper: (ChartData data, _) => data.x,
                                  yValueMapper: (ChartData data, _) => data.y,
                                )
                              ]),
                        )
                      ],
                    ),
                  )),
              Positioned(
                  top: MediaQuery.of(context).size.height * 0.50,
//
                  child: Row(
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.42,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            // color: Color.fromARGB(255, 0, 0, 0),
                          ),
                          child: GridView.count(
                            primary: false,
                            padding: const EdgeInsets.all(10),
                            mainAxisSpacing: 20,
                            crossAxisCount: 4,
                            children: <Widget>[
                              InkWell(
                                splashColor: Colors.white,
                                highlightColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AllCustomers()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/customer.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Customer',
                                      presetFontSizes: [
                                        13,
                                        10,
                                        20
                                      ], //][10,16,20]
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AllSuppliers()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/supplier.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Supplier',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FkScreen()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/product.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Product',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PreHome()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/preOrder.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'PreOrder',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DeliveryFee()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/delivery.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Delivery',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CouponListScreen()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/coupon.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Cupon',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              PositionListScreen()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/position.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Position',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AllStaff()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/staff.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Staff',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Settings()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/setting.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Setting',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              LanguagePage()));
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/language.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Language',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                highlightColor: Colors.white,
                                splashColor: Colors.white,
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) => _logoutDialog());
                                },
                                child: Column(
                                  children: [
                                    Image.asset(
                                      'assets/logout.png',
                                      width: MediaQuery.of(context).size.width *
                                          0.16,
                                    ),
                                    const AutoSizeText(
                                      'Logout',
                                      presetFontSizes: [13, 10, 20],
                                    )
                                  ],
                                ),
                              ),
                              // InkWell(
                              //   highlightColor: Colors.white,
                              //   splashColor: Colors.white,
                              //   onTap: () {

                              //   },
                              //   child: Column(
                              //     children: [
                              //       Image.asset(
                              //         'assets/staff.png',
                              //         width: MediaQuery.of(context).size.width *
                              //             0.16,
                              //       ),
                              //       const AutoSizeText(
                              //         'Logout',
                              //         presetFontSizes: [13, 10, 20],
                              //       )
                              //     ],
                              //   ),
                              // ),
                            ],
                          )),
                    ],
                  )),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color.fromARGB(255, 236, 236, 236),
        //selectedItemColor: Color.fromARGB(255, 255, 0, 0),
        unselectedItemColor: Color.fromARGB(255, 0, 0, 0).withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        currentIndex: _currentIndex,
        onTap: (value) {
          // Respond to item press.
          setState(() {
            _currentIndex = value;
          });
          if (value == 0) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => POSScreen()));
          } else if (value == 1) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => OrderHistory()));
          } else if (value == 2) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Report()));
          } else {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => MainExpense()));
          }
        },
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/secondPos.png',
              width: 39,
            ),
            label: 'Pos'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/secondAllOrder.png',
              width: 39,
            ),
            label: 'All Order'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/secondReport.png',
              width: 39,
            ),
            label: 'Report',
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/secondExpense.png',
              width: 39,
            ),
            label: 'Expense',
          ),
        ],
      ),
    );
  }

  Widget _logoutDialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Center(
          child: Text(
        'Logout',
        style: TextStyle(fontSize: 20),
      )),

      // content: Padding(
      //   padding: const EdgeInsets.symmetric(horizontal: 55),
      //   child: Text("Have a nice day?"),
      // ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: 80,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(backgroundColor: mainColor),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Cancel")),
            ),
            SizedBox(
              width: 80,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(backgroundColor: mainColor),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("OK")),
            )
          ],
        )
      ],
    );
  }
}

class ChartData {
  ChartData(this.x, this.y);
  final String x;
  final double? y;
}

final gradientList = <List<Color>>[
  [
    Color.fromRGBO(223, 250, 92, 1),
    Color.fromRGBO(129, 250, 112, 1),
  ],
  [
    Color.fromRGBO(129, 182, 205, 1),
    Color.fromRGBO(91, 253, 199, 1),
  ],
  [
    Color.fromRGBO(175, 63, 62, 1.0),
    Color.fromRGBO(254, 154, 92, 1),
  ]
];
