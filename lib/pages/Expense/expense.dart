import 'package:flutter/material.dart';
import 'package:micro_pos/pages/Expense/addExpense.dart';
import 'package:micro_pos/pages/Report/report.dart';



class MainExpense extends StatefulWidget {
  const MainExpense({super.key});

  @override
  State<MainExpense> createState() => _MainExpenseState();
}

class _MainExpenseState extends State<MainExpense> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: mainColor,
        title: Text("Expense"),
      ),
      body: Column(
        children: [
           Form(
            
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                  
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 15),
                      suffixIcon: Icon(Icons.search),
                      hintText: "Search here...",
                      filled: true,
                      
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none))),
            ),
          ),
          Container(
            
            color: Colors.white,
            width: 350,
            height: 120,
            child: Row(
              children: [
                Column(
                  
                  children: [
                    Image.asset('assets/report6.jpg',height: 120,),
                  ],
                ),
                Container(
                  width: 150,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 5,),
                      Text("Watch",style: TextStyle(fontSize: 24,color: mainColor),),
                      SizedBox(height: 5,),
                      Text("expense:     1000ks"),
                      SizedBox(height: 5,),
                      Text("2023-04-28   10:38PM"),
                      SizedBox(height: 5,),
                      Text("Note: test"),
                      
                    ],
                    
                  ),
                  
                ),
                Column(
                  children: [
                    SizedBox(height: 30),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFFD9D9D9),
                        shape: CircleBorder()
                      ),
                      onPressed: (){
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Center(child: Text('DELETE!')),
                              content: Text('Are you sure you want to delete?'),
                              actions: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {},
                                        child: Text('Ok'),
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor:
                                                Color(0xff002D71))),
                                    ElevatedButton(
                                        onPressed: () {},
                                        child: Text('Cancel'),
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Color(0xff002D71)))
                                  ],
                                )
                              ],
                            );
                          });

                    }, child: Icon(Icons.delete,color: Colors.red,
                    ),),
                    
                  ],
                ),
                
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>AddExpense()));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}