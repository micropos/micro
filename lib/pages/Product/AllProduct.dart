import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Product/AddProduct.dart';
import 'package:micro_pos/pages/Product/FkScreen.dart';
import 'package:micro_pos/pages/Product/ProductDetail.dart';

class ProductPages extends StatefulWidget {
  String? name;
  String? stock;
  String? sell;

  ProductPages({super.key, this.name, this.stock, this.sell});

  @override
  State<ProductPages> createState() => _ProductPagesState();
}

class _ProductPagesState extends State<ProductPages> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final category = ['Phone', 'Laptop', 'Keyboard', 'Charger', 'Earphone'];
  String? valueCategories;
  TextEditingController _productTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product'),
        elevation: 0,
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.all(10.0),
                height: 60,
                child: TextFormField(
                  controller: _productTextController,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      hintText: 'Search Here...',
                      hintStyle: TextStyle(height: 2.8),
                      filled: true,
                      fillColor: fillColor,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none)),
                ),
              ),
            ),
            Container(
              height: 45,
              margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: fillColor,
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: const Text('Categories'),
                  value: valueCategories,
                  icon: const Icon(Icons.arrow_drop_down),
                  isExpanded: true,
                  items: category.map(cashMenu).toList(),
                  onChanged: (value) => setState(
                    () => this.valueCategories = value,
                  ),
                ),
              ),
            ),
            // FkScreen()
            // Expanded(
            //   child: ListView.builder(
            //       itemCount: productList.length,
            //       itemBuilder: (context, index) => _productDetail(
            //             productList[index].name.toString(),
            //             productList[index].stock.toString(),
            //             productList[index].sell.toString(),
            //             index,
            //           )),
            // ),

            // ListTile(
            //   title: Text("Hello"),
            //   trailing: Icon(Icons.person),
            // )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => AddProduct()));
        },
      ),
    );
  }

//   Widget _productDetail(String name, String stock, String sell, int index) {
//     return Container(
//       padding: EdgeInsets.all(8.0),
//       height: 120,
//       child: Card(
//         color: fillColor,
//         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//         child: Row(
//           children: [
//             Padding(
//               padding: EdgeInsets.symmetric(horizontal: 0),
//               child: SizedBox(
//                 // height: 200,
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.only(
//                       topLeft: Radius.circular(10),
//                       bottomLeft: Radius.circular(10)),
//                   child: Image.asset('assets/01.png'),
//                   // child: Container(
//                   //                     width: 38,
//                   //                     height: 38,
//                   //                     decoration: BoxDecoration(
//                   //                         image: prod.user!.image != null
//                   //                             ? DecorationImage(
//                   //                                 image: NetworkImage(
//                   //                                     '${.user!.image}'))
//                   //                             : null,
//                   //                         borderRadius:
//                   //                             BorderRadius.circular(25),
//                   //                         color: Colors.blue),
//                   //                   ),
//                 ),
//               ),
//             ),
//             SizedBox(
//               width: 5,
//             ),
//             Expanded(
//               child: Padding(
//                 padding: EdgeInsets.all(5),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text(
//                       name,
//                       style: TextStyle(
//                           fontWeight: FontWeight.w500,
//                           color: mainColor,
//                           fontSize: 18),
//                     ),
//                     Text(
//                       'Stock : $stock',
//                       style: TextStyle(color: Colors.black, fontSize: 15),
//                     ),
//                     Text(
//                       'Sell Price : Ks $sell',
//                       style: TextStyle(color: Colors.black, fontSize: 15),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//             PopupMenuButton(
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(20)),
//               child: Padding(
//                 padding: EdgeInsets.only(right: 1),
//                 child: Icon(Icons.more_vert),
//               ),
//               itemBuilder: (context) => [
//                 PopupMenuItem(
//                   child: ListTile(
//                     minLeadingWidth: 10,
//                     title: Text("Edit"),
//                     leading: Icon(Icons.edit),
//                   ),
//                   value: 'edit',
//                 ),
//                 PopupMenuItem(
//                   child: ListTile(
//                     minLeadingWidth: 10,
//                     title: Text("Detail"),
//                     leading: Icon(Icons.details),
//                   ),
//                   value: 'detail',
//                 ),
//                 PopupMenuItem(
//                   child: ListTile(
//                     minLeadingWidth: 10,
//                     title: Text("Delete"),
//                     leading: Icon(Icons.delete),
//                   ),
//                   value: 'delete',
//                 ),
//               ],
//               onSelected: (value) {
//                 if (value == 'edit') {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => AddProduct()));
//                 } else if (value == 'detail') {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => ProductDetail()));
//                 } else if (value == 'delete') {
//                   showCupertinoDialog(
//                       context: context,
//                       builder: (context) => _deleteDialog(index));
//                 }
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }

  // CupertinoAlertDialog _deleteDialog(int index) {
  //   return CupertinoAlertDialog(
  //     title: Text(
  //       "Delete!",
  //       style: TextStyle(color: Colors.red),
  //     ),
  //     content: Text("Are you sure you want to delete"),
  //     actions: [
  //       CupertinoDialogAction(
  //         child: Text("Cancel"),
  //         onPressed: () {
  //           Navigator.of(context).pop();
  //         },
  //       ),
  //       CupertinoDialogAction(
  //         child: Text("OK"),
  //         onPressed: () {
  //           productList.remove(index);
  //         },
  //       )
  //     ],
  //   );
  // }

  DropdownMenuItem<String> cashMenu(String cash) => DropdownMenuItem(
        value: cash,
        child: Text(
          cash,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
