import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';

import 'AddProduct.dart';
import 'ProductDetail.dart';

class FkScreen extends StatefulWidget {
  FkScreen({super.key});

  @override
  State<FkScreen> createState() => _FkScreenState();
}

class _FkScreenState extends State<FkScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<Map> productList = [
    {
      'name': 'Iphone 14 Pro Max',
      'stock': '34',
      'sell': 'Ks 4560000',
      'image': 'assets/p1.jpg'
    },
    {
      'name': 'MacBook Air 2020',
      'stock': '50',
      'sell': 'Ks 6160000',
      'image': 'assets/p2.jpg'
    },
    {
      'name': 'Canon EOS R50 Camera',
      'stock': '70',
      'sell': 'Ks 2356000',
      'image': 'assets/p3.jpg'
    },
    {
      'name': 'Fast Charger',
      'stock': '100',
      'sell': 'Ks 136000',
      'image': 'assets/p5.jpg'
    },
    {
      'name': 'Hair Dryer',
      'stock': '90',
      'sell': 'Ks 56000',
      'image': 'assets/p6.jpg'
    },
    {
      'name': 'Magic Mouse',
      'stock': '120',
      'sell': 'Ks 12000',
      'image': 'assets/mouse.jpeg'
    },
    {
      'name': 'Red Mi',
      'stock': '120',
      'sell': 'Ks 500000',
      'image': 'assets/androidPhone.jpg'
    },
    {
      'name': 'HeadPhone',
      'stock': '120',
      'sell': 'Ks 50000',
      'image': 'assets/hphone.png'
    },
    {
      'name': 'Desktop Box',
      'stock': '120',
      'sell': 'Ks 12000',
      'image': 'assets/desktop_box.jpg'
    },
    {
      'name': 'Air Pod',
      'stock': '120',
      'sell': 'Ks 12000',
      'image': 'assets/airPod.jpeg'
    },
  ];

  TextEditingController _productTextController = TextEditingController();
  final category = ['Phone', 'Laptop', 'Keyboard', 'Charger', 'Earphone'];
  String? valueCategories;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product'),
        elevation: 0,
        backgroundColor: mainColor,
      ),
      body: Column(
        children: [
          Column(
            children: [
              Container(
                child: Form(
                  key: _formKey,
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    height: 60,
                    child: TextFormField(
                      controller: _productTextController,
                      decoration: InputDecoration(
                          suffixIcon: Icon(Icons.search),
                          hintText: 'Search Here...',
                          hintStyle: TextStyle(height: 2.8),
                          filled: true,
                          fillColor: fillColor,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide.none)),
                    ),
                  ),
                ),
              ),
              Container(
                height: 45,
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                padding: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: fillColor,
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    hint: const Text('Categories'),
                    value: valueCategories,
                    icon: const Icon(Icons.arrow_drop_down),
                    isExpanded: true,
                    items: category.map(categoryMenu).toList(),
                    onChanged: (value) => setState(
                      () => this.valueCategories = value,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
              children: productList
                  .map((e) => Product(
                      name: e['name'],
                      stock: e['stock'],
                      sell: e['sell'],
                      image: e['image']))
                  .toList(),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => AddProduct()));
        },
      ),
    );
  }

  DropdownMenuItem<String> categoryMenu(String category) => DropdownMenuItem(
        value: category,
        child: Text(
          category,
          style: const TextStyle(fontSize: 16),
        ),
      );
}

class Product extends StatefulWidget {
  String name, stock, sell, image;

  Product(
      {super.key,
      required this.name,
      required this.stock,
      required this.sell,
      required this.image});

  @override
  State<Product> createState() => _ProductState();
}

class _ProductState extends State<Product> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Card(
      color: fillColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            child: Image.asset(
              widget.image,
              width: 80,
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    widget.name,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        color: mainColor,
                        fontSize: 18),
                  ),
                  Text(
                    'Stock : ${widget.stock}',
                    style: const TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  Text(
                    'Sell Price : ${widget.sell}',
                    style: const TextStyle(color: Colors.black, fontSize: 15),
                  )
                ],
              ),
            ),
          ),
          PopupMenuButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Padding(
              padding: EdgeInsets.only(right: 1),
              child: Icon(Icons.more_vert),
            ),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: ListTile(
                  minLeadingWidth: 10,
                  title: Text("Edit"),
                  leading: Icon(Icons.edit),
                ),
                value: 'edit',
              ),
              PopupMenuItem(
                child: ListTile(
                  minLeadingWidth: 10,
                  title: Text("Detail"),
                  leading: Icon(Icons.details),
                ),
                value: 'detail',
              ),
              PopupMenuItem(
                child: ListTile(
                  minLeadingWidth: 10,
                  title: Text("Delete"),
                  leading: Icon(Icons.delete),
                ),
                value: 'delete',
              ),
            ],
            onSelected: (value) {
              if (value == 'edit') {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddProduct()));
              } else if (value == 'detail') {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProductDetail(image: widget.image,name1: widget.name,name2: widget.stock,name3: widget.sell,)));
              } else if (value == 'delete') {
                //
              }
            },
          )
        ],
      ),
    );
  }
}
