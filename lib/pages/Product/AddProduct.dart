import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/Model/productFakeModel.dart';
import 'package:micro_pos/pages/Product/AllProduct.dart';

class AddProduct extends StatefulWidget {
  const AddProduct({super.key});

  @override
  State<AddProduct> createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _productName = TextEditingController();
  final TextEditingController _productCode = TextEditingController();
  final TextEditingController _productCategory = TextEditingController();
  final TextEditingController _productDiecription = TextEditingController();
  final TextEditingController _productStock = TextEditingController();
  final TextEditingController _productBuyPrice = TextEditingController();
  final TextEditingController _unitProductSellPrice = TextEditingController();
  final TextEditingController _productWeight = TextEditingController();
  final TextEditingController _selectProductWeightUnit =
      TextEditingController();
  final TextEditingController _selectSupplier = TextEditingController();
  final TextEditingController _tax = TextEditingController();

  File? _imageFile;
  final _picker = ImagePicker();

  Future getImage() async {
    final PickedFile = await _picker.pickImage(source: ImageSource.gallery);
    if (PickedFile != null) {
      setState(() {
        _imageFile = File(PickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Product'),
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // const Padding(
                      //   padding: EdgeInsets.all(5),
                      //   child: Text(
                      //     'Product Name',
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      const SizedBox(height: 10),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Name"
                            : null,
                        controller: _productName,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.person),
                            hintText: "Product Name",
                            // helperText: "Product Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Code",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      SizedBox(
                        height: 5,
                      ),
                      // Row(
                      //   children: [
                      //     TextFormField(
                      //     validator: (value) => value!.isEmpty
                      //         ? "Please Enter Your Product Code"
                      //         : null,
                      //     controller: _productCode,
                      //     decoration: InputDecoration(
                      //       //contentPadding: EdgeInsets.only(left: 20),
                      //         filled: true,
                      //         fillColor: fillColor,
                      //         border: OutlineInputBorder(
                      //             borderSide: BorderSide.none,
                      //             borderRadius: BorderRadius.circular(10)),
                      //         prefixIcon: Icon(Icons.qr_code),
                      //         hintText: "Product Code",
                      //         helperStyle:
                      //             TextStyle(fontWeight: FontWeight.bold)),
                      //   ),
                      //   Padding(
                      //     padding: const EdgeInsets.only(right: 5.0),
                      //     child: Image.asset('assets/preBarcode.png'),
                      //   )
                      //   ],
                      // ),
                     
                          TextFormField(
                            validator: (value) => value!.isEmpty
                                ? "Please Enter Your Product Code"
                                : null,
                            controller: _productCode,
                            decoration: InputDecoration(
                              //contentPadding: EdgeInsets.only(left: 20),
                                filled: true,
                                fillColor: fillColor,
                                border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.circular(10)),
                                prefixIcon: Icon(Icons.qr_code),
                                hintText: "Product Code",
                                helperStyle:
                                    TextStyle(fontWeight: FontWeight.bold)),
                          ),
                          // trailing: Padding(
                          //   padding: const EdgeInsets.only(right: 5.0),
                          //   child: Image.asset('assets/preBarcode.png'),
                          // )),
                      

                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Category",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Category"
                            : null,
                        controller: _productCategory,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.category),
                            hintText: "Product Category",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Description",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Description"
                            : null,
                        controller: _productDiecription,
                        keyboardType: TextInputType.multiline,
                        maxLines: 6,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Padding(
                              padding: const EdgeInsets.only(bottom: 95),
                              child: Icon(Icons.description),
                            ),
                            hintText: "Product Description",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),

                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Stock",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Stock"
                            : null,
                        controller: _productStock,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.inventory_2),
                            hintText: "Product Stock",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Buy Price",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Buy Price"
                            : null,
                        controller: _productBuyPrice,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.price_change),
                            hintText: "Product Buy Price",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Unit Product Sell Price",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Unit Product Sell Price"
                            : null,
                        controller: _unitProductSellPrice,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.price_change),
                            hintText: "Unit Product Sell Price",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Product Weight",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Product Weight"
                            : null,
                        controller: _productWeight,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.monitor_weight),
                            hintText: "Product Weight",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Sell Product Weight Unit",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Sell Product Weight Unit"
                            : null,
                        controller: _selectProductWeightUnit,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.monitor_weight),
                            hintText: "Sell Product Weight Unit",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Text(
                      //     "Select Supplier",
                      //     style: TextStyle(fontSize: 18),
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 5,
                      // ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Please Enter Your Select Supplier"
                            : null,
                        controller: _selectSupplier,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.storefront),
                            hintText: "Select Supplier",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Please Enter Your Tax" : null,
                        controller: _tax,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: fillColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            prefixIcon: Icon(Icons.attach_money),
                            hintText: "Tax",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        // child: SizedBox(
                        //     width: double.infinity,
                        //     child: kTextButton("Choose Product Image", () {
                        //       if (_formKey.currentState!.validate()) {}
                        //     })),
                        child: SizedBox(
                          width: double.infinity,
                          child: TextButton(
                              child: Text(
                                'Choose Product image',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  // getImage();
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) => mainColor),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          height: 200,
                          decoration: BoxDecoration(
                              image: _imageFile == null
                                  ? null
                                  : DecorationImage(
                                      image: FileImage(_imageFile ?? File('')),
                                      fit: BoxFit.cover)),
                          child: Center(
                              child: IconButton(
                            icon: Icon(
                              Icons.image,
                              size: 50,
                              color: Colors.black38,
                            ),
                            onPressed: () {
                              getImage();
                            },
                          )),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: SizedBox(
                            width: double.infinity,
                            child: TextButton(
                              child: Text(
                                "Add",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  productList.add(
                                    Product(
                                      name: _productName.text,
                                      stock: _productStock.text,
                                      sell: _unitProductSellPrice.text,
                                    ),
                                  );
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ProductPages(
                                          // name: _productName.text,

                                          ),
                                    ),
                                  );
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) => mainColor),
                              ),
                            ),
                          )),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
