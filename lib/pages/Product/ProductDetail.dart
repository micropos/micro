import 'dart:math';

import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';

class ProductDetail extends StatefulWidget {
  final String? image;
  final String? name1;
  final String? name2;
  final String? name3;
  ProductDetail({this.image, this.name1, this.name2,this.name3});
  // const ProductDetail({super.key});

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Detail'),
        backgroundColor: mainColor,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Center(
              child: Card(
                child: Image.asset(widget.image ?? ''),
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: SizedBox(
                width: 250,
                child: Card(
                    elevation: 0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Product Name : ${widget.name1}'),
                        Text('Product Code : 35423579'),
                        Text('Product Category : Category'),
                        Text('Product Description : Gook Product'),
                        Text('Product Stock :${widget.name2}'),
                        Text('Product Buy Price : ${widget.name3}'),
                        Text('Unit Product Sell Price : 50000'),
                        Text('Product Weight : 23.6kg'),
                        Text('Select Product Weight Unit : kg'),
                        Text('Select Supplier : Name Store'),
                        Text('Tax : 3000'),
                      ],
                    )),
              ))
        ],
      ),
    );
  }
}
