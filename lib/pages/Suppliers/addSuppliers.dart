import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/Model/fakeModel.dart';
import 'package:micro_pos/Model/fakeModel2.dart';
import 'package:micro_pos/pages/Suppliers/allSuppliers.dart';

class AddSuppliers extends StatefulWidget {
  final String? title,suppliersButton;
  AddSuppliers({this.title,this.suppliersButton});
  //const AddSuppliers({super.key});

  @override
  State<AddSuppliers> createState() => _AddSuppliersState();
}

class _AddSuppliersState extends State<AddSuppliers> {
  File? _imageFile;
  final _pickedImage = ImagePicker();

  Future getImage() async {
    final imagePick = await _pickedImage.pickImage(source: ImageSource.gallery);
    if (imagePick != null) {
      setState(() {
        _imageFile = File(imagePick.path);
      });
    }
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _supplierNameController = TextEditingController();
  TextEditingController _supplierContactController = TextEditingController();
  TextEditingController _supplierPhoneController = TextEditingController();
  TextEditingController _supplierEmailController = TextEditingController();
  TextEditingController _supplierAddressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.title}'),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Supplier Name",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Supplier Name can't be empty"
                            : null,
                        controller: _supplierNameController,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.shopping_cart),
                            hintText: "Supplier Name",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Supplier Contact Person",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) => value!.isEmpty
                            ? "Supplier Contact Name can't be empty"
                            : null,
                        controller: _supplierContactController,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.person),
                            hintText: "Supplier Contact Person",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Supplier Phone",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) => value!.length < 9
                            ? "Phone have must least 9 characters"
                            : null,
                        controller: _supplierPhoneController,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.phone),
                            hintText: "Supplier Phone",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Supplier Email",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Email can't be empty" : null,
                        controller: _supplierEmailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.phone),
                            hintText: "Supplier Email",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Supplier Address",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      TextFormField(
                        validator: (value) =>
                            value!.isEmpty ? "Address can't be empty" : null,
                        controller: _supplierAddressController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 6,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: textFieldColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Padding(
                              padding: const EdgeInsets.only(bottom: 95),
                              child: Icon(Icons.location_history_outlined),
                            ),
                            hintText: "Supplier Address",
                            //helperText: "Customer Name",
                            helperStyle:
                                TextStyle(fontWeight: FontWeight.bold)),
                      )
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                "Supplier Photo",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: BoxDecoration(
                    image: _imageFile == null
                        ? null
                        : DecorationImage(
                            image: FileImage(_imageFile ?? File('')),
                            fit: BoxFit.cover),
                    color: textFieldColor,
                    borderRadius: BorderRadius.circular(20)),
                //padding: EdgeInsets.all(10.0),
                width: double.infinity,
                height: 220,
                child:_imageFile == null ? GestureDetector(
                    onTap: () {
                      getImage();
                    },
                    child: Image.asset('assets/addImage.png')) : null
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                  width: double.infinity,
                  child: kAddButton('${widget.suppliersButton}', () {
                    if (_formKey.currentState!.validate()) {
                     supplierList.add(Supplier(
                        shopName: _supplierNameController.text,
                        supplierName: _supplierContactController.text,
                        phone: _supplierPhoneController.text,
                        email: _supplierEmailController.text,
                        address: _supplierEmailController.text,
                        image: _imageFile

                      ));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllSuppliers()));
                    }
                  })),
            )
          ],
        ),
      ),
    );
  }
}
