import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/Suppliers/addSuppliers.dart';
import 'package:micro_pos/pages/response.dart';
import 'package:sizer/sizer.dart';

class AllSuppliers extends StatefulWidget {
  const AllSuppliers({super.key});

  @override
  State<AllSuppliers> createState() => _AllSuppliersState();
}

class _AllSuppliersState extends State<AllSuppliers> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _suppliersTextFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("All Suppliers"),
        centerTitle: false,
        backgroundColor: mainColor,
      ),
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextFormField(
                  controller: _suppliersTextFieldController,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      suffixIcon: Icon(Icons.search),
                      hintText: "Search here...",
                      filled: true,
                      fillColor: textFieldColor,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide.none))),
            ),
          ),
          // _secondSupplier(mSize),
          Expanded(
            child: ListView.builder(
                itemCount: supplierList.length,
                itemBuilder: (context, index) => _supplierCard(
                      File(supplierList[index].image!.path),
                      supplierList[index].shopName.toString(),
                      supplierList[index].supplierName.toString(),
                      supplierList[index].phone.toString(),
                      supplierList[index].email.toString(),
                      supplierList[index].address.toString(),

                      // index,

                      // customerList[index].image.toString(),
                    )),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: mainColor,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddSuppliers(
                        title: "Add Suppliers",
                        suppliersButton: "Add",
                      )));
        },
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _supplierCard(File image, String shopName, String name, String phone,
      String email, String address) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        //height: Response.isSmallScreen(context)? msizes * 0.25 : msizes * 0.21,
        // width: msizes.width,
        // height: 40.h,
        // width: 150.h,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: Color(0xffF3F1F1),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 180,
                    height: 140,
                    child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        child: Image.file(
                          image,
                          fit: BoxFit.cover,
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Color(0xffD9D9D9),
                              shape: CircleBorder()),
                          onPressed: () {},
                          child: Icon(
                            Icons.phone,
                            color: Colors.green,
                          )),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Color(0xffD9D9D9),
                              shape: CircleBorder()),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddSuppliers(
                                          title: "Edit Supplier",
                                          suppliersButton: "Edit",
                                        )));
                          },
                          child: Icon(
                            Icons.edit,
                            color: mainColor,
                          )),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Color(0xffD9D9D9),
                              shape: CircleBorder()),
                          onPressed: () {
                            showCupertinoDialog(
                                context: context,
                                builder: (context) => _deleteDialog());
                          },
                          child: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ))
                    ],
                  ),
                ],
              ),
              SizedBox(
                width: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        shopName,
                        style: TextStyle(
                            color: mainColor,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        name,
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 150,
                        child: Text(
                          phone,
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 150,
                        child: Text(
                          email,
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 150,
                        child: Text(
                          address,
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _secondSupplier(sizes) {
    return Container(
      //height: Response.isSmallScreen(context)? msizes * 0.25 : msizes * 0.21,
      // width: msizes.width,
      // width: sizes.width,
      width: 330,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Color(0xffF3F1F1),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 180,
                  height: 140,
                  // decoration: BoxDecoration(
                  //   color: Colors.black,
                  //     borderRadius: BorderRadius.only(
                  //         topLeft: Radius.circular(20),
                  //         bottomRight: Radius.circular(20))),
                  child: Image.asset(
                    'assets/suppliersPhoto.png',
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xffD9D9D9),
                            shape: CircleBorder()),
                        onPressed: () {},
                        child: Icon(
                          Icons.phone,
                          color: Colors.green,
                        )),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xffD9D9D9),
                            shape: CircleBorder()),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddSuppliers(
                                        title: "Edit Supplier",
                                        suppliersButton: "Edit",
                                      )));
                        },
                        child: Icon(
                          Icons.edit,
                          color: mainColor,
                        )),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xffD9D9D9),
                            shape: CircleBorder()),
                        onPressed: () {
                          showCupertinoDialog(
                              context: context,
                              builder: (context) => _deleteDialog());
                        },
                        child: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ))
                  ],
                ),
              ],
            ),
            SizedBox(
              width: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Login Union",
                      style: TextStyle(
                          color: mainColor,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Mg Mg",
                      style: TextStyle(color: Colors.black, fontSize: 18),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      // width: 150,
                      child: Text(
                        "09700900800",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      // width: 150,
                      child: Text(
                        "mgmg@gmail.com",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: 150,
                      child: Text(
                        "Yangon,Thamine",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  CupertinoAlertDialog _deleteDialog() {
    return CupertinoAlertDialog(
      title: Text(
        "Delete!",
        style: TextStyle(color: Colors.red),
      ),
      content: Text("Are you sure you want to delete"),
      actions: [
        CupertinoDialogAction(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        CupertinoDialogAction(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
