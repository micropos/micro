import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:micro_pos/Components/constants.dart';

class ProductCartScreen extends StatefulWidget {
  const ProductCartScreen({super.key});

  @override
  State<ProductCartScreen> createState() => _ProductCartScreenState();
}

class _ProductCartScreenState extends State<ProductCartScreen> {
  List<String> customer = [
    'Mg Mg',
    'Kyaw Kyaw',
    'Su Su',
    'Aung Aung',
    'Phyo Phyo'
  ];
  List<String> pickUp = [
    'Pick Up',
    'Home Delivery',
    'Completed',
    'Cancelled',
    'Failed',
    'Expired',
    'Refuned'
  ];
  var macCounter = 1;
  var clothCounter = 1;
  final cash = ['KBZ PAY', 'WAVE PAY', 'CB PAY', 'AYA PAY'];
  String? valueCustomer;
  String? _checkOutSelected = "Pick Up";
  String? valueCash;
  //var navPop = Navigator.of(context).pop;

  void _showDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text(
              'DELETE!',
              textAlign: TextAlign.center,
            ),
            content: const Text(
              'Are you sure you want to delete.',
              style: TextStyle(fontSize: 15),
              textAlign: TextAlign.center,
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Cancel',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(mainColor)),
                child: const Text(
                  'Delete',
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var navPop = Navigator.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainColor,
        title: const Text('Product Cart'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _getCustomer(),
            const SizedBox(
              height: 5,
            ),
            _getProductCart(),
            _getProductCartOne(),
            const SizedBox(
              height: 20,
            ),
            _subTotal(navPop),
          ],
        ),
      ),
    );
  }

  Widget _getProductCart() {
    return Card(
      child: Container(
        margin: const EdgeInsets.all(10),
        width: 407,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Stack(
          children: [
            Image.asset('assets/cart.png'),
            const Positioned(
              top: 10,
              left: 120,
              child: Text('Macbook Pro'),
            ),
            const Positioned(
              top: 40,
              left: 120,
              child: Text('Ks2000000'),
            ),
            Positioned(
              bottom: 60,
              right: 0,
              child: IconButton(
                onPressed: () {
                  _showDialog();
                },
                icon: const Icon(Icons.delete),
                color: Colors.red,
                splashColor: Colors.white,
                highlightColor: Colors.white,
              ),
            ),
            Positioned(
                left: 120,
                top: 70,
                child: SizedBox(
                  width: 30,
                  height: 30,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        backgroundColor: mainColor,
                        shape: CircleBorder()),
                    onPressed: () {
                      setState(() {
                        if (macCounter > 0) {
                          macCounter -= 1;
                        }
                      });
                    },
                    child: Icon(Icons.remove),
                  ),
                )),
            Positioned(
                //width 30,height 30,circular 20
                right: 175,
                top: 70,
                child: SizedBox(
                  width: 30,
                  height: 30,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        backgroundColor: mainColor,
                        shape: CircleBorder()),
                    onPressed: () {
                      setState(() {
                        macCounter += 1;
                      });
                    },
                    child: Icon(Icons.add),
                  ),
                )),
            Positioned(
              top: 73,
              left: 160,
              child: Text(
                '$macCounter',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getCustomer() {
    return Container(
      width: 380,
      height: 50,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200],
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          hint: const Text('Customer'),
          value: valueCustomer,
          icon: const Icon(Icons.arrow_drop_down),
          isExpanded: true,
          items: customer.map(customerMenu).toList(),
          onChanged: (value) => setState(
            () => this.valueCustomer = value,
          ),
        ),
      ),
    );
  }

  Widget _getProductCartOne() {
    return SingleChildScrollView(
      child: Card(
        child: Container(
          margin: const EdgeInsets.all(10),
          width: 407,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Image.asset('assets/cloths.png'),
              const Positioned(
                top: 10,
                left: 120,
                child: Text('Cloth'),
              ),
              const Positioned(
                top: 40,
                left: 120,
                child: Text('Ks200000'),
              ),
              Positioned(
                bottom: 60,
                right: 0,
                child: IconButton(
                  onPressed: () {
                    _showDialog();
                  },
                  icon: const Icon(Icons.delete),
                  color: Colors.red,
                  splashColor: Colors.white,
                  highlightColor: Colors.white,
                ),
              ),
              Positioned(
                  left: 120,
                  top: 70,
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.zero,
                          backgroundColor: mainColor,
                          shape: CircleBorder()),
                      onPressed: () {
                        setState(() {
                          if (clothCounter > 0) {
                            clothCounter -= 1;
                          }
                        });
                      },
                      child: Icon(Icons.remove),
                    ),
                  )),
              Positioned(
                  right: 175,
                  top: 70,
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.zero,
                          backgroundColor: mainColor,
                          shape: CircleBorder()),
                      onPressed: () {
                        setState(() {
                          clothCounter += 1;
                        });
                      },
                      child: Icon(Icons.add),
                    ),
                  )),
              Positioned(
                top: 73,
                left: 160,
                child: Text(
                  '$clothCounter',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getDivider() {
    return Divider(
      height: 50,
      color: Colors.grey[300],
      thickness: 1,
      indent: 10,
      endIndent: 10,
    );
  }

  Widget _subTotal(navPop) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey[100],
      ),
      margin: const EdgeInsets.only(left: 10, right: 8),
      height: 315,
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 55,
            child: Form(
              child: Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                child: TextFormField(
                    decoration: InputDecoration(
                        hintText: "Discount",
                        hintStyle: const TextStyle(height: 1),
                        filled: true,
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none))),
              ),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 55,
            child: Form(
              child: Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                child: TextFormField(
                    decoration: InputDecoration(
                        hintText: "Delivery Fee",
                        hintStyle: const TextStyle(height: 3),
                        filled: true,
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none))),
              ),
            ),
          ),
          const SizedBox(
            height: 0,
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Sub Total'),
              trailing: Text('2252000Ks'),
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Total Tax'),
              trailing: Text('2000Ks'),
            ),
          ),
          Container(
            width: 380,
            height: 30,
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: const ListTile(
              title: Text('Total'),
              trailing: Text('2000Ks'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: 200,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: mainColor,
              ),
              onPressed: () {
                showModalBottomSheet(
                    isScrollControlled: true,
                    context: context,
                    builder: (BuildContext context) {
                      return StatefulBuilder(builder: (context, stateDropDwon) {
                        return SizedBox(
                          height: 400,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                      onPressed: () {
                                        navPop.pop();
                                      },
                                      icon: const Icon(Icons.close),
                                      iconSize: 30,
                                    ),
                                    const SizedBox(
                                      width: 240,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                                  actions: [
                                                    TextButton(
                                                      onPressed: () {
                                                        navPop.pop();
                                                        Navigator.of(context)
                                                            .pop();
                                                        Fluttertoast
                                                        
                                                            .showToast(
                                                              
                                                              msg: "Order Success",
                                                              timeInSecForIosWeb: 1,
                                                              // gravity: ToastGravity.BOTTOM,
                                                              backgroundColor: Colors.green
                                                            );
                                                      },
                                                      child:
                                                          const Text('Close'),
                                                    ),
                                                  ],
                                                  title: const Text(
                                                    'ORDER SUCCESS!',
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  content: Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 25),
                                                    child: Image.asset(
                                                        'assets/true.png',
                                                        width: 100,
                                                        height: 100),
                                                  ),
                                                ));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 18),
                                        child: Text(
                                          'Submit',
                                          style: TextStyle(fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: 380,
                                height: 50,
                                margin: const EdgeInsets.only(
                                    left: 10, right: 10, top: 10),
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 12,
                                  vertical: 4,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.grey[200],
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    hint: Text('$_checkOutSelected'),
                                    value: _checkOutSelected,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    items: pickUp.map((e) {
                                      return DropdownMenuItem(
                                        child: Text(e),
                                        value: e,
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      stateDropDwon(() {
                                        _checkOutSelected = value!;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 380,
                                height: 50,
                                margin: const EdgeInsets.only(
                                    left: 10, right: 10, top: 10),
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 12,
                                  vertical: 4,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.grey[200],
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    hint: const Text('Cash'),
                                    value: valueCash,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    items: cash.map(cashMenu).toList(),
                                    onChanged: (value) => stateDropDwon(
                                      () => this.valueCash = value,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 380,
                                height: 30,
                                margin:
                                    const EdgeInsets.only(left: 10, right: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: const ListTile(
                                  title: Text('Sub Total'),
                                  trailing: Text('2252000Ks'),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 380,
                                height: 30,
                                margin:
                                    const EdgeInsets.only(left: 10, right: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: const ListTile(
                                  title: Text('Total Tax'),
                                  trailing: Text('2000Ks'),
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Divider(
                                height: 5,
                                color: Colors.grey[300],
                                thickness: 1,
                                indent: 10,
                                endIndent: 10,
                              ),
                              const ListTile(
                                title: Padding(
                                  padding: EdgeInsets.only(left: 27),
                                  child: Text('Total'),
                                ),
                                trailing: Padding(
                                  padding: EdgeInsets.only(right: 20),
                                  child: Text('2254000Ks'),
                                ),
                              ),
                            ],
                          ),
                        );
                      });
                    });
              },
              child: const Text('CHECKOUT'),
            ),
          )
        ],
      ),
    );
  }

  DropdownMenuItem<String> customerMenu(String customer) => DropdownMenuItem(
        value: customer,
        child: Text(
          customer,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> pickUpMenu(String pickUp) => DropdownMenuItem(
        value: pickUp,
        child: Text(
          pickUp,
          style: const TextStyle(fontSize: 16),
        ),
      );

  DropdownMenuItem<String> cashMenu(String cash) => DropdownMenuItem(
        value: cash,
        child: Text(
          cash,
          style: const TextStyle(fontSize: 16),
        ),
      );
}
