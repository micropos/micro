import 'package:add_to_cart_animation/add_to_cart_animation.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Components/constants.dart';
import 'package:micro_pos/pages/POS/PosPage/product_cart_screen.dart';

class POSScreen extends StatefulWidget {
  const POSScreen({super.key});

  @override
  State<POSScreen> createState() => _POSScreenState();
}

class _POSScreenState extends State<POSScreen> {
  // int addCount = 0;

  var _cartQuantityItems = 0;
  GlobalKey<CartIconKey> _cartKey = GlobalKey<CartIconKey>();
  late Function(GlobalKey) runAddToCartAnimation;
  @override
  Widget build(BuildContext context) {
    return AddToCartAnimation(
      cartKey: _cartKey,
      height: 30,
      width: 30,
      opacity: 0.85,
      dragAnimation: const DragToCartAnimationOptions(
        rotation: true,
      ),
      jumpAnimation: const JumpAnimationOptions(),
      createAddToCartAnimation: (runAddToCartAnimation) {
        // You can run the animation by addToCartAnimationMethod, just pass trough the the global key of  the image as parameter
        this.runAddToCartAnimation = runAddToCartAnimation;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: mainColor,
          title: const Text('POS'),
          actions: [
            // Stack(children: [
            //   Padding(
            //     padding: const EdgeInsets.all(10.0),
            //     child: IconButton(
            //       onPressed: () {},
            //       icon: const Icon(Icons.shopping_cart),
            //       iconSize: 25,
            //     ),
            //   ),
            //   addCount <= 0
            //       ? Text('')
            //       : Positioned(
            //           top: 5,
            //           right: 7,
            //           child: SizedBox(
            //             height: 25,
            //             width: 25,
            //             child: ElevatedButton(
            //                 style: ElevatedButton.styleFrom(
            //                     backgroundColor: Colors.white,
            //                     shape: CircleBorder(),
            //                     padding: EdgeInsets.zero),
            //                 onPressed: () {},
            //                 child: Text(
            //                   "$addCount",
            //                   style: TextStyle(color: Colors.black),
            //                 )),
            //           ),
            //         )
            // ])

            AddToCartIcon(
              key: _cartKey,
              icon: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductCartScreen()));
                  },
                  child: Icon(Icons.shopping_cart)),
              badgeOptions:
                  BadgeOptions(active: true, backgroundColor: Colors.red),
            ),
            SizedBox(
              width: 16,
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 50,
              child: ListTile(
                title: Form(
                  child: TextFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          suffixIcon: const Icon(Icons.search),
                          hintText: "Search here...",
                          hintStyle: const TextStyle(height: 1),
                          filled: true,
                          fillColor: Color.fromARGB(34, 158, 158, 158),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide.none))),
                ),
                trailing: TextButton(
                    onPressed: () {},
                    child: Image.asset(
                      'assets/preBarcode.png',
                      width: 60,
                      height: 60,
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Product Category',
                    style: TextStyle(fontSize: 16),
                  ),
                  // const SizedBox(
                  //   width: 115,
                  // ),
                  ElevatedButton.icon(
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(mainColor),
                    ),
                    onPressed: () {},
                    icon: const Text('Reset'),
                    label: const Icon(Icons.refresh),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: SizedBox(
                height: 90,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/laptop.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Laptop'),
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/phone.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Phone'),
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/cloth.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Cloth'),
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/food.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Food'),
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/sport.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Sport'),
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mainColor,
                          padding: const EdgeInsets.all(0)),
                      onPressed: () {},
                      child: Row(
                        children: [
                          Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.asset(
                                  'assets/health.png',
                                  width: 80,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text('Health'),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Row(
                children: const [
                  Text(
                    'All Product',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
         
            Expanded(
              child: GridView.count(
                crossAxisCount: 2,
                children: [
                  ...List.generate(
                      listPhotos.length,
                      (index) => SecondPage(
                            onClick: _listClick,
                            index: index,
                            images: listPhotos[index].image,
                            names: listPhotos[index].name,
                            prices: listPhotos[index].price,
                          ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _getGridView(String image, String pName, String price) {
  //   return SizedBox(
  //     width: 200,
  //     height: 300,
  //     child: Card(
  //       child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [
  //           Stack(
  //             clipBehavior: Clip.none,
  //             children: [
  //               ClipRRect(
  //                 borderRadius: BorderRadius.circular(5),
  //                 child: Image.asset(
  //                   image,
  //                 ),
  //               ),
  //               Positioned(
  //                   //width 40,height 40,circular 50
  //                   //original wrap with stack
  //                   top: 105,
  //                   left: 140,
  //                   child: SizedBox(
  //                     width: 40,
  //                     height: 40,
  //                     child: ElevatedButton(
  //                       style: ElevatedButton.styleFrom(
  //                           padding: EdgeInsets.zero,
  //                           backgroundColor: Colors.amber,
  //                           shape: CircleBorder()),
  //                       child: Icon(Icons.add),
  //                       onPressed: () {
  //                         setState(() {
  //                           addCount += 1;
  //                         });
  //                       },
  //                     ),
  //                   )),
  //               ClipRRect(
  //                 borderRadius:
  //                     const BorderRadius.only(topLeft: Radius.circular(3)),
  //                 child: Container(
  //                   width: 70,
  //                   height: 20,
  //                   color: Colors.blue,
  //                   child: const Padding(
  //                     padding: EdgeInsets.only(left: 10),
  //                     child: Text(
  //                       'In Stock',
  //                       style: TextStyle(color: Colors.white),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //           SizedBox(
  //             height: 18,
  //           ),
  //           Padding(
  //             padding: const EdgeInsets.only(left: 15),
  //             child: Text(
  //               pName,
  //               style: TextStyle(fontSize: 16),
  //             ),
  //           ),
  //           SizedBox(
  //             height: 13,
  //           ),
  //           Padding(
  //             padding: const EdgeInsets.only(left: 15),
  //             child: Text(
  //               price,
  //               style: TextStyle(fontSize: 16),
  //             ),
  //           )
  //         ],
  //       ),
  //     ),
  //   );
  // }
  void _listClick(GlobalKey widgetKey) async {
    await runAddToCartAnimation(widgetKey);
    await _cartKey.currentState!
        .runCartAnimation((++_cartQuantityItems).toString());
  }
}

class SecondPage extends StatefulWidget {
  final int index;
  final void Function(GlobalKey) onClick;
  String? names;
  String? images;
  String? prices;
  int? stock;

  SecondPage(
      {super.key,
      required this.onClick,
      required this.index,
      this.names,
      this.images,
      this.prices,
      this.stock});
  //const SecondPage({super.key});

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final GlobalKey widgetKey = GlobalKey();
  int _stockCount = 10;
  @override
  Widget build(BuildContext context) {
    Container _imageContainer = Container(
      key: widgetKey,
      //color: Colors.black,
      width: 200,
      height: 120,
      child: GestureDetector(
        onTap: () {
          if (_stockCount > 0) {
            setState(() {
              _stockCount--;
            });
            widget.onClick(widgetKey);
          }
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.asset(
            // widget.images ?? '',
            widget.images ?? '',
            width: 100,
            height: 90,
          ),
        ),
      ),
    );
    return Container(
      width: 200,
      height: 200,
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                Center(
                  child: _imageContainer,
                ),
                _stockCount > 0 ?
                ClipRRect(
                  borderRadius:
                      const BorderRadius.only(topLeft: Radius.circular(3)),
                  child: Container(
                    width: 70,
                    height: 20,
                    color: Colors.blue,
                    child: const Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'In Stock',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ) : 
                ClipRRect(
                  borderRadius:
                      const BorderRadius.only(topLeft: Radius.circular(3)),
                  child: Container(
                    width: 100,
                    height: 20,
                    color: Colors.red,
                    child: const Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Out of Stock',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 140,
                  top: 95,
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.only(left: 8),
                            backgroundColor: Color(0xff002D71),
                            shape: CircleBorder()),
                        onPressed: () {
                          if (_stockCount > 0) {
                            setState(() {
                              _stockCount--;
                            });
                            widget.onClick(widgetKey);
                          }
                        },
                        label: Text(''),
                        icon: Icon(
                          Icons.add,
                          color: Colors.white,
                        )),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                widget.names ?? '',
                style: TextStyle(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                "Stock : ${_stockCount}",
                style: TextStyle(fontSize: 16),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                widget.prices ?? '',
                style: TextStyle(fontSize: 16),
              ),
            )
          ],
        ),
      ),
    );
  }
}
