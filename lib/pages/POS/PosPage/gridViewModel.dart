class GridViewModel {
  String? image;
  String? productType;
  String? price;

  GridViewModel({this.image, this.productType, this.price});
}
