import 'package:get/get.dart';

class LocaleString extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'Pos': 'Pos',
          'All Order': 'All Order',
        },
        'mm_MM': {
          'Pos': 'Pos',
          'All Order': 'မှာယူမှုအားလုံး',
        },
        'ch_CH': {
          'Pos': 'Pos',
          'All Order': 'All Order',
        }
      };
}
