import 'package:flutter/material.dart';
import 'package:micro_pos/Changelanguage/LacaleString.dart';
import 'package:micro_pos/pages/Coupon/coupon_screen.dart';
import 'package:micro_pos/pages/Expense/expense.dart';
import 'package:micro_pos/pages/PreOrder/PreHome.dart';
import 'package:micro_pos/pages/Report/ExpenseGraph/expense.dart';
import 'package:micro_pos/pages/Report/SaleGraph/salegraph.dart';
import 'package:micro_pos/pages/Report/report.dart';
import 'package:micro_pos/pages/Staff/allStaff.dart';
import 'package:micro_pos/pages/AllOrders/order_history.dart';
import 'package:micro_pos/pages/Customer/addCustomer.dart';
import 'package:micro_pos/pages/Customer/allCustomer.dart';
import 'package:micro_pos/pages/DeliveryFee/deliveryAdd.dart';
import 'package:micro_pos/pages/DeliveryFee/deliveryfee.dart';
import 'package:micro_pos/pages/Languages/language.dart';
import 'package:micro_pos/pages/Main/HomePage.dart';
import 'package:micro_pos/pages/POS/PosPage/pos_screen.dart';
import 'package:micro_pos/pages/POS/PosPage/product_cart_screen.dart';
import 'package:micro_pos/pages/Product/AllProduct.dart';
import 'package:micro_pos/pages/Product/FkScreen.dart';

import 'package:micro_pos/pages/Settings/Payment/addPayment.dart';
import 'package:micro_pos/pages/Settings/Payment/payment.dart';
import 'package:micro_pos/pages/Settings/addCategories.dart';
import 'package:micro_pos/pages/Settings/categories.dart';
import 'package:micro_pos/pages/Settings/editCategories.dart';
import 'package:micro_pos/pages/Settings/settings.dart';
import 'package:micro_pos/pages/Suppliers/addSuppliers.dart';
import 'package:micro_pos/pages/Suppliers/allSuppliers.dart';
import 'package:micro_pos/pages/microLogin.dart';
import 'package:micro_pos/pages/microSplash.dart';
import 'package:get/get.dart';

void main() {
  var materialApp = GetMaterialApp(
    debugShowCheckedModeBanner: false,
    translations: LocaleString(),
    locale: Locale('en', 'US'),
    initialRoute: '/',
    routes: {
      '/': (context) => MicroLogin(),
      '/splash': (context) => MicroSplash(),
      '/main': (context) => HomePage(),
      '/allCustomer': (context) => AllCustomers(),
      '/addCustomer': (context) => AddCustomer(),
      '/allSupplier': (context) => AllSuppliers(),
      '/addSuppliet': (context) => AddSuppliers(),
      '/setting': (context) => Settings(),
      '/categoreis': (context) => Categories(),
      '/addCat': (context) => AddCategories(),
      '/edit': (context) => EditCategories(),
      '/payment': (context) => PayMent(),
      '/addPay': (context) => AddPayment(),
      '/preOrder': (context) => PreHome(),
      '/order': (context) => OrderHistory(),
      '/deliverFee': (context) => DeliveryFee(),
      '/deliveryadd': (context) => DeliveryAdd(),
      '/report': (context) => Report(),
      '/language': (context) => LanguagePage(),
      '/possScreen': (context) => POSScreen(),
      '/productCart': (context) => ProductCartScreen(),
      '/mainExpense': (context) => MainExpense(),
      '/saleGraph': (context) => SaleGraph(),
      '/expenseGraph': (context) => ExpenseGraph(),
      '/pp': (context) => ProductPages(),
      '/fk': (context) => FkScreen(),
      '/allS': (context) => AllStaff(),
      '/couponScreen': (context) => CouponScreen()
    },
  );
  runApp(materialApp);
}
