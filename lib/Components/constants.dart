import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_pos/Model/deliveryModel.dart';
import 'package:micro_pos/Model/fakeModel2.dart';
import 'package:micro_pos/Model/posModel.dart';
import 'package:micro_pos/Model/staffMoel.dart';

import '../Model/fakeModel.dart';
import '../pages/POS/PosPage/gridViewModel.dart';

const preColor = const Color(0XFF03600a);
const mainColor = Color(0xff002D71);
const textFieldColor = Color.fromARGB(34, 158, 158, 158);
const buttonColor = Color.fromARGB(255, 198, 222, 254);
const buttonTextColor = Color(0xff0A82FF);
const fillColor = Color.fromARGB(255, 244, 240, 240);
const preFill = Color.fromARGB(255, 237, 241, 236);

InputDecoration inputDecorations(String label, Icon icon) {
  return InputDecoration(
      filled: true,
      fillColor: textFieldColor,
      prefixIcon: icon,
      hintText: label,
      border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(20)));
}

List<Customer> customerList = [];
List<Supplier> supplierList = [];
List<dynamic> productList = [];

CupertinoButton kAddButton(String label, Function onTap) {
  return CupertinoButton(
      //minSize: double.infinity,
      color: mainColor,
      child: Text("$label"),
      onPressed: () => onTap());
}

CupertinoButton PreAddButton(String label, Function onTap) {
  return CupertinoButton(
      //minSize: double.infinity,
      color: preColor,
      child: Text("$label"),
      onPressed: () => onTap());
}

String chageImage(File file) {
  if (file == null) {
    null;
  }
  return base64Encode(file.readAsBytesSync());
}

List<GridViewModel> gridList = [
  GridViewModel(
      image: 'assets/laptop.png',
      productType: "Macbook Pro",
      price: "2300000Ks"),
  GridViewModel(
      image: 'assets/cloth.png', productType: "Cloth", price: "50000Ks"),
  GridViewModel(image: 'assets/posIce.png', productType: "Ice", price: "200Ks"),
  GridViewModel(
      image: 'assets/posBicycle.png', productType: "Bicycle", price: "50000Ks")
];

List<StaffModel> staffList = [];

final listPhotos = [
  Model(
      image: 'assets/phone.png', name: "HeadPhone", price: "2300000", stock: 5),
  Model(image: 'assets/laptop.png', name: 'MMouse', price: '50000', stock: 5),
  Model(image: 'assets/cloth.png', name: "Iphone", price: "20000", stock: 5),
  Model(
      image: 'assets/posIce.png',
      name: "Macbook Air",
      price: "30000",
      stock: 5),
  Model(
      image: 'assets/posBicycle.png', name: 'Canon', price: '90000', stock: 5),
//   Model(image: 'assets/phone.png', name: "HeadPhone", price: '80000', stock: 5),
//   Model(image: 'assets/phone.png', name: 'Desktop Box', price: "100000", stock: 5),
//   Model(image: 'assets/phone.png', name: 'Hair Dryer', price: "3000", stock: 5),
//   Model(
//       image: 'assets/phone.jpg', name: "RedMi", price: "4000", stock: 5),
//   Model(image: 'assets/phone.jpg', name: "Shoulder", price: "5000", stock: 5),
];

List<DeliveryModel> deliveyList = [
  DeliveryModel(city: "Yangon", townShip: 'Sule', fees: "3000KS"),
  DeliveryModel(city: "Mandalay", townShip: 'Chanmyathazi', fees: "5000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Thamine', fees: "2000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Bahan', fees: "4000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Sanchaung', fees: "8000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Bahan', fees: "9000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Thamine', fees: "10000KS"),
  DeliveryModel(city: "Mandalay", townShip: 'Chanmyathazi', fees: "4500KS"),
  DeliveryModel(city: "Yangon", townShip: 'Sule', fees: "25000KS"),
  DeliveryModel(city: "Yangon", townShip: 'Thamine', fees: "10000KS"),
  DeliveryModel(city: "Mandalay", townShip: 'Chanmyathazi', fees: "4500KS"),
  DeliveryModel(city: "Yangon", townShip: 'Sule', fees: "25000KS"),
];

CupertinoAlertDialog cupertinoDialog(context) {
  return CupertinoAlertDialog(
    title: Text(
      "Delete!",
      style: TextStyle(fontSize: 20, color: Colors.red),
    ),
    content: Text("Are you sure you want to delete?"),
    actions: [
      CupertinoDialogAction(
        child: Text("Cancel"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      CupertinoDialogAction(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      )
    ],
  );
}

AlertDialog simpleDialog(context) {
  return AlertDialog(
    title: const Text(
      'DELETE!',
      textAlign: TextAlign.center,
    ),
    content: const Text(
      'Are you sure you want to delete.',
      style: TextStyle(fontSize: 15),
      textAlign: TextAlign.center,
    ),
    actions: [
      ElevatedButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(mainColor)),
        child: const Text(
          'Cancel',
        ),
      ),
      const SizedBox(
        width: 30,
      ),
      ElevatedButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(mainColor)),
        child: const Text(
          'Delete',
        ),
      ),
      const SizedBox(
        width: 30,
      ),
    ],
  );
}

