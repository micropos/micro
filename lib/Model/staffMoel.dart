import 'dart:io';

class StaffModel {
  String? name;
  String? password;
  String? position;
  File? image;

  StaffModel({this.name, this.password, this.position,this.image});
}
