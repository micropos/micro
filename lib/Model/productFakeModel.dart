class Product {
  int? id;
  String? name;
  String? stock;
  String? sell;

  Product({
    this.id,
    this.name,
    this.stock,
    this.sell,
  });
}
