class Model {
  String? image;
  String? name;
  String? price;
  int? stock;

  Model({this.image, this.name, this.price,this.stock});
}
