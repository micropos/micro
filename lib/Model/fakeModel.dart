import 'dart:io';

class Customer {
  String? name;
  String? phone;
  String? email;
  String? address;
  File? image;
  String? city;
  String? townShip;

  Customer({this.name, this.phone, this.email, this.address, this.image,this.city,this.townShip});
}
