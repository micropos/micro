import 'dart:io';

class Supplier {
  String? shopName;
  String? supplierName;
  String? phone;
  String? email;
  String? address;
  File? image;

  Supplier(
      {this.shopName,
      this.supplierName,
      this.phone,
      this.email,
      this.address,
      this.image});
}
